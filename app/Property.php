<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'name', 'ptd_code', 'postcode', 'address1', 'address2',
        'city', 'state', 'attn_name', 'attn_phone_number',

        'propertytype_id'
    ];
}
