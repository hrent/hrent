<?php

namespace App\Http\Controllers;
use App;
use Auth;
use Illuminate\Http\Request;
use View;
use App\Unit;
use App\Operator;
use DB;
use App\Insurance;
use App\Billplz;

class CheckoutController extends Controller
{
    public function __construct() {
        if(empty($_SERVER['HTTP_HOST'])){
            $_SERVER['HTTP_HOST'] = 'stage';
        }
        //dev
        $callback_dev = 'http://'.$_SERVER['HTTP_HOST'].'/api/v2/invoice-callback/';
        $return_dev = 'http://'.$_SERVER['HTTP_HOST'].'/bilpllz/payinsurance-return/';
        $passback_dev = 'http://stage.intentapp.in';
        //production 
        $callback_pro = 'https://'.$_SERVER['HTTP_HOST'].'/api/v2/invoice-callback/';
        $return_pro = 'https://'.$_SERVER['HTTP_HOST'].'/bilpllz/payinsurance-return/';
        $passback_pro = 'http://app.intentapp.in';

        if (App::environment('production')) {
              $this->billplz_callback = $callback_pro;
              $this->billplz_return = $return_pro;
              $this->billplz_passback = $passback_pro;
            }else{
                $this->billplz_callback = $callback_dev;
                $this->billplz_return = $return_dev;
                $this->billplz_passback = $passback_dev;
            }
        $this->month = array("1" => "January", "2" => "February", "3" => "March", "4" => "April", "5" => "May", "6" => "June", "7" => "July", "8" => "August","9" => "September", "10" => "October", "11" => "November", "12" => "December");
    }

    // return checkout index page
    public function index(Request $request)
    {
        return view('checkout.index');
    }
    // return checkout index page
    public function getcheckout(Request $request)
    {
        $session_data = $request->session()->get('current_checkout_data');
        $response_data = $session_data['response_data'];
        $total_amount   =  $session_data['total'];
        $tax_amount     =  $session_data['tax_amount'];
        
        foreach($response_data as $key=>$val){
            $units[] = DB::table('units')
            ->where('unit_number', $key)
            ->select(
                'id', 'block_number', 'unit_number', 'address', 'label', 'remarks',
                'id AS value',
                DB::raw('CONCAT(IFNULL(block_number,""),IF(block_number IS NULL,""," - "),unit_number,IF(address IS NULL,"", " - "),IFNULL(address,"")) AS label')
            )
            ->first();
        }
        $checkout['checkoutlist'] = $units;
        foreach($session_data['plans'] as $key=>$val){
            $details_plans[] = DB::table('happyrent_insurance_plans')
            ->where('insurance_plan_id', $val)
            ->select(
                'happyrent_insurance_plans.insurance_plan_id', 'happyrent_insurance_plans.insurance_plan_name', 'happyrent_insurance_plans.insurance_plan_price', 'happyrent_insurance_plans.insurance_plan_duration','happyrent_insurance_plans.insurance_plan_status'
            )
            ->orderBy('happyrent_insurance_plans.insurance_plan_name')
            ->first();
       }
       $checkout['plans']           = $details_plans;
       $checkout['total_amount']    = $total_amount+$tax_amount;
       $checkout['tax_amount']      = $tax_amount;
       return $checkout;
    }
    //create a bill for insulcare units
    public function createinsurance(Request $request){
        $call_response= false;
        $secret_array = $request->all();
       
        $session_data = $request->session()->get('current_checkout_data');

        //get the setting details
        $setting_details = DB::table('happyrent_site_setting')
        ->select(
            'happyrent_site_setting.billplz_secret_key',
            'happyrent_site_setting.billplz_signature',
            'happyrent_site_setting.billplz_collection_id', 
            'happyrent_site_setting.tax_amount'
        )->first();

        $response_data = $session_data['response_data'];
        $total_amount =  $session_data['total'];
        $total_paybale_amout = $total_amount + $setting_details->tax_amount;
        
        // store details into pending checkout and use it after payment success
       foreach($secret_array as $key => $insured_unit){
            DB::table('pending_checkout_details')->insert([
                'block_number'          => $insured_unit['block_number'],
                'unit_number'           => $insured_unit['unit_number'],
                'address'               => $insured_unit['address'],
                'label'                 => $insured_unit['label'],
                'value'                 => $insured_unit['value'],
                'insurance_id'          => $insured_unit['value'],
                'tax_amout'             => $setting_details->tax_amount,
                'sub_total'             => $total_amount,
                'total_amount'          => $total_paybale_amout,
                'created_at'            => date('Y-m-d H:i:s'),
                'updated_at'            => date('Y-m-d H:i:s'),
            
            ]);
        }
        $billplz = new Billplz($setting_details->billplz_secret_key, $setting_details->billplz_signature);
        $valid_collection_id = $billplz->check_collection_id($setting_details->billplz_collection_id);

        if($valid_collection_id){
            $name = Auth::user()->name;
            $billplz->setName(Auth::user()->name);
            $billplz->setReference_1_Label('Refrenece no.');
            $billplz->setReference_1(time());
            $billplz->setAmount($total_paybale_amout);
            $billplz->setMobile(Auth::user()->phone_number); 
            $billplz->setEmail(Auth::user()->email); 
            
            $description ='INSURANCE PLAN PURCHASE';
            
            $billplz->setDescription($description);
            $billplz->setPassbackURL($this->billplz_passback);
            $billplz->setCollection($setting_details->billplz_collection_id);//get from database for onlt one send only one collection for pay
            $billplz->setDue(date('Y-m-d'));
            $billplz->setPassbackURL($this->billplz_callback, $this->billplz_return);
            $billplz->create_bill(); 
            $billid = $billplz->getID();
            if($billid){
                return $call_response = response()->json(['response'=> true, 'bill_url' => $billplz->getURL(), 'bill_id' => $billid ,'callback' => $this->billplz_callback]);
            }
        }
        return $call_response= false;
    }
    //this function for insert details after comes from return billplz
    public function returnbill(Request $request){
        $session_data           = $request->session()->get('current_checkout_data');
        $session_response_data  = $session_data['plans'];
        $billplz_return         = $request->all();
        $toekn_data             = DB::table('pending_checkout_details')->get();
        $bill_id                = $billplz_return['billplz']['id'];
        
        if($bill_id){
            foreach($toekn_data as $key => $insured_unit){
                foreach($session_response_data as $key => $val){
                    if($key == $insured_unit->unit_number ){
                        $plan_ids[] = $val;
                    }
                    
                }
            }
            $plan_ids_arr = json_encode($plan_ids);
            $insurance_transaction_id = DB::table('transactions')->insertGetId([
                'insurance_billplz_id'          => $bill_id,
                'insurance_plan_id'             => $plan_ids_arr,
                'insurance_subtotal_amount'     => $session_data['total'],
                'insurance_taxable_amount'      => $session_data['tax_amount'],
                'insurance_transaction_status'  => 1,
                'insurance_payment_by'          => 'fpx',
                'insurance_transaction_date'    => date('Y-m-d H:i:s'),
                'insurance_total_amount'        => $session_data['total']+$session_data['tax_amount'],
                'created_at'                    => date('Y-m-d H:i:s'),
                'updated_at'                    => date('Y-m-d H:i:s'),
              
            ]);
            
            $transation = DB::table('transactions')
            ->where('insurance_transaction_id', $insurance_transaction_id)
            ->select(
                'transactions.insurance_transaction_status',
                'transactions.insurance_plan_id',
                'transactions.insurance_transaction_date'                     
            )->first();

           
            $plan_index = 0;
            foreach($toekn_data as $key => $insured_unit){
                DB::table('happyrent_insurances')->insert([
                    'insurance_unit_id'         => $insured_unit->insurance_id,
                    'insurance_plan_id'         => $plan_ids[$plan_index],
                    'insurance_transaction_id'  => $insurance_transaction_id,
                    'insurance_date'            => $transation->insurance_transaction_date,
                    'insurance_status'          => $transation->insurance_transaction_status,
                    'created_at'                => date('Y-m-d H:i:s'),
                    'updated_at'                => date('Y-m-d H:i:s'),
                  
                ]);
                $plan_index++;
            }
            DB::table('pending_checkout_details')->truncate(); //delete data after user
        }
        $request->session()->forget('current_checkout_data');//delete session
        return redirect('/success');
    }
    public function success(){
        return view('checkout.response');

    }
}