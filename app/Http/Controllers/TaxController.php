<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tax;
use DB;

class TaxController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    // return user index page
    public function index()
    {
        
       
        return view('tax.index');
    }

    // retrieve users by given filter
    public function getTaxesApi()
    {
        $perpage = request('perpage');

        $data = DB::table('taxes')
        //->where('status',1)
        ->select(
            'taxes.tax_id','taxes.name', 'taxes.percentage','taxes.deleted'
        );
        $data = $this->filterTaxesApi($data);

        if($perpage != 'All') {
            $data = $data->paginate($perpage);
        }else {
            $data = $data->get();
        }
       return $data;
    }
 // get all users api
    public function getAllTaxesApi()
    {
         $taxes = DB::table('taxes')
        //->where('status',1)
        ->select(
            'taxes.tax_id','taxes.name', 'taxes.percentage','taxes.deleted'
        )
            ->orderBy('taxes.name')
            ->get();

        return $taxes;
    }
     // store or update new individual user
    public function storeUpdateTaxApi(Request $request)
    {
        $data = $request->all();
        
      

        $this->validate(request(), [
            'name' => 'required',
            'percentage' => 'required',
            
        ]);
        

        $fieldsArr = [
            'name' => request('name'),
            'percentage' => request('percentage'),
           
            
        ];

        if(request('tax_id')) {

           
            DB::table('taxes')
            ->where('tax_id', request('tax_id'))
            ->update($fieldsArr);
           
           
        }else {
              
          Tax::create([
                    'name'           => request('name'),
                    'percentage'     => request('percentage'),
                   	'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s'),
                ]);
            $last_id = DB::getPDO()->lastInsertId();
           
            $data['tax_id'] =  $last_id;
        }
        $data['error'] = 0;
        return $data;
    }

     // delete single user api(int user_id)
    public function deactivateSingleTaxApi($tax_id)
    {
        
        DB::table('taxes')
            ->where('tax_id', $tax_id)
            ->update(['deleted' => 1]);

        $response = array(
                'status' => 'success',
              
        );
       
    }
    public function restoreSingleTaxApi($tax_id)
    {
        
        DB::table('taxes')
            ->where('tax_id', $tax_id)
            ->update(['deleted' => 0]);

        $response = array(
                'status' => 'success',
              
        );
       
    }
     public function changeTaxStatus(Request $request)
    {
         $data =$request->all();

        if($data['status'] == 1){ $status = 0;}else{ $status = 1;}
        DB::table('taxes')
            ->where('tax_id', $data['tax_id'])
            ->update(['status' => $status]);

        $response = array(
                'status' => 'success',
                'status' =>$status,
              
        );
       
    }

     // users api filter(Query query)
    private function filterTaxesApi($query)
    {
        $name = request('name');
        $percentage = request('percentage');
       
        $sortkey = request('sortkey');
        $reverse = request('reverse');

        if($name) {
            $query = $query->where('taxes.name', 'LIKE', '%'.$name.'%');
        }
        if($percentage) {
            $query = $query->where('taxes.percentage', 'LIKE', '%'.$percentage.'%');
        }
       
        if($sortkey) {
            $query = $query->orderBy($sortkey, $reverse == 'true' ? 'asc' : 'desc');
        }else{
            $query = $query->orderBy('taxes.name');
        }

        return $query;
    }
    
}
