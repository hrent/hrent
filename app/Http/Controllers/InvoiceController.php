<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Invoice;
use DB;
use Illuminate\Http\RedirectResponse;
use Config;
class InvoiceController extends Controller
{
        
    public function __construct()
    {
        $this->middleware('auth');
    }
    // return invoice index page
    public function index()
    {
       return view('invoice.index')->with('currency',Config::get('app.currency'));
    }
    
    // Retrieve all invoices and send to invoice list page
    public function getAllInvocies()
    {   
        $perpage = request('perpage');
        $invoices = DB::table('invoices')
        ->leftJoin('properties', 'properties.id', '=', 'invoices.property_id')
        ->leftJoin('units', 'units.id', '=', 'invoices.unit_id')
        ->select(
            'invoices.invoice_id','invoices.property_id','invoices.unit_id', 'invoices.sender_id', 'invoices.bill_date', 'invoices.due_date', 'invoices.note', 'invoices.last_email_sent_date',
            'invoices.status', 'invoices.recurring','invoices.recurring_invoice_id','invoices.repeat_every','invoices.repeat_type','invoices.no_of_cycles','invoices.next_recurring_date','invoices.no_of_cycles_completed',
            'invoices.due_reminder_date','invoices.recurring_reminder_date','invoices.discount_amount','invoices.cancelled_at',
            'invoices.cancelled_by', 'invoices.deleted', 'invoices.subtotal_amount','invoices.taxable_amount','invoices.payable_amount','invoices.is_rental',
            'properties.name','properties.propertytype_id','units.block_number','units.unit_number'
        );

        $invoices = $this->filterInvoicesApi($invoices);
        if($perpage != 'All') {
            $invoices = $invoices->paginate($perpage);
        }else {
            $invoices = $invoices->get();
        }
        return $invoices;
    }
    // Store or update Invoices
    public function storeUpdateInvoiceApi(Request $request)
    {   
        $subtotal_amount = 0.00;$payable_amount = 0.00;
        $invoice_details    = $request->all();
        $tenancies          = DB::table('tenancies')
        ->where('unit_id',request('unit_id'))
        ->leftJoin('tenants', 'tenants.id', '=', 'tenancies.tenant_id')
        ->select(
            'tenancies.id','tenancies.rental','tenancies.tenant_id AS tenancie_tenant_id',
            'tenants.name','tenants.email','tenants.phone_number'
        )->first();
        if ($tenancies) {
            $tenancies_id = $tenancies->id;
            
        }else{
            $tenancies_id = null;
        }
        //checkent taxes are select or not
        if (isset($invoice_details['tax']) || isset($invoice_details['tax_two'])) {
            if (isset($invoice_details['tax']) && isset($invoice_details['tax_two'])) {
                $tax_one_details = DB::table('taxes')->where('tax_id',$invoice_details['tax'])->select('tax_id','name', 'percentage')->first();
                $tax_two_details = DB::table('taxes')->where('tax_id',$invoice_details['tax_two'])->select('tax_id','name', 'percentage')->first();
               
                $taxable_amount = $tax_one_details->percentage+$tax_two_details->percentage;
            }
            if (isset($invoice_details['tax']) && !isset($invoice_details['tax_two']) ) {
                $tax_one_details = DB::table('taxes')->where('tax_id',$invoice_details['tax'])->select('tax_id','name', 'percentage')->first();
               
                $taxable_amount = $tax_one_details->percentage;
            }
            if (isset($invoice_details['tax_two']) && !isset($invoice_details['tax'])) {
                $tax_two_details = DB::table('taxes')->where('tax_id',$invoice_details['tax_two'])->select('tax_id','name', 'percentage')->first();
               
                $taxable_amount = $tax_two_details->percentage;
            }
            
        }else{
            $taxable_amount =  0.00;
        }
        $taxable_amount =  sprintf("%.2f", $taxable_amount); 
        $this->validate(request(), [
            'bill_date'     => 'required',
            'due_date'      => 'required',
            'property_id'   => 'required',
            'unit_id'       => 'required'
            
        ]);
        if (isset($invoice_details['recuring']) ) {
            $fieldsArr = [
                'bill_date'         =>  request('bill_date'),
                'due_date'          =>  request('due_date'),
                'property_id'       =>  request('property_id'),
                'unit_id'           =>  request('unit_id'),
                'tenancy_id'        =>  $tenancies_id,
                'recuring'          =>  request('recuring'),
                'repeat_every'      =>  request('repeat_every'),
                'repeat_type'       =>  request('repeat_type'),
                'no_of_cycles'      =>  request('no_of_cycles'),
                'note'              =>  request('note'),
                'sender_id'         =>  Auth::user()->id,
                'subtotal_amount'   =>  $subtotal_amount,
                'taxable_amount'    =>  $taxable_amount,
                'payable_amount'    =>  $payable_amount,

            ];
        }else{
            $fieldsArr = [
                'bill_date'         =>  request('bill_date'),
                'due_date'          =>  request('due_date'),
                'property_id'       =>  request('property_id'),
                'unit_id'           =>  request('unit_id'),
                'tenancy_id'        =>  $tenancies_id,
                'sender_id'         =>  Auth::user()->id,
                'subtotal_amount'   =>  $subtotal_amount,
                'taxable_amount'    =>  $taxable_amount,
                'payable_amount'    =>  $payable_amount,
            ];
        }
        
        if(isset($invoice_details['invoice_id'])) {
            $invoice_id = $invoice_details['invoice_id'];
            DB::table('invoices')
            ->where('invoice_id', $invoice_details['invoice_id'])
            ->update($fieldsArr);
        }else {
            $invoice_id = Invoice::create([
                'bill_date'         =>  request('bill_date'),
                'due_date'          =>  request('due_date'),
                'property_id'       =>  request('property_id'),
                'unit_id'           =>  request('unit_id'),
                'tenancy_id'        =>  $tenancies_id,
                'sender_id'         =>  Auth::user()->id,
                'created_at'        =>  date('Y-m-d H:i:s'),
                'updated_at'        =>  date('Y-m-d H:i:s'),
                'subtotal_amount'   =>  $subtotal_amount,
                'taxable_amount'    =>  $taxable_amount,
                'payable_amount'    =>  $payable_amount,
            ]);
           
            
        }
        $response = array('status' => 'success' , 'redirect_to' =>'invoice/view/'.$invoice_id->id );
        return $response;
    }
    //Add and Update of Invoices Items bu this function
    public function storeUpdateInvoiceItem(Request $request)
    {
        $invoice_item_details    = $request->all();
        $amount = 0.00;
        $item_details = DB::table('items')->where('item_id',$invoice_item_details['form']['item_id'])->select('items.item_id','items.title','items.unit_type','items.item_code')->first();
        if (isset($invoice_item_details['form']['tenancy'])) {
            $tanancy_arr = explode('_', $invoice_item_details['form']['tenancy']);
            $amount      = $tanancy_arr[0];
        }
        
        if (isset($invoice_item_details['form']['tax']) || isset($invoice_item_details['form']['tax_two'])) {
            if (isset($invoice_item_details['form']['tax']) && isset($invoice_item_details['form']['tax_two'])) {
                $tax_one_details    = DB::table('taxes')->where('tax_id',$invoice_item_details['form']['tax'])->select('tax_id','name', 'percentage')->first();
                $tax_two_details    = DB::table('taxes')->where('tax_id',$invoice_item_details['form']['tax_two'])->select('tax_id','name', 'percentage')->first();
                $taxable_amount     = $tax_one_details->percentage + $tax_two_details->percentage;
            }
            if (isset($invoice_item_details['form']['tax']) && !isset($invoice_item_details['form']['tax_two']) ) {
                $tax_one_details = DB::table('taxes')->where('tax_id',$invoice_item_details['form']['tax'])->select('tax_id','name', 'percentage')->first();
                $taxable_amount     = $tax_one_details->percentage;
            }
            if (isset($invoice_item_details['form']['tax_two']) && !isset($invoice_item_details['form']['tax'])) {
                $tax_two_details = DB::table('taxes')->where('tax_id',$invoice_item_details['form']['tax_two'])->select('tax_id','name', 'percentage')->first();
                $taxable_amount = $tax_two_details->percentage;
            }
        }else{
            $taxable_amount =  0.00;
        }
        if (isset($invoice_item_details['form']['description'])) {
            $description = $invoice_item_details['form']['description'];
        }else{
            $description ='';
        }
        $invoiceitems = DB::table('invoiceitems')->insertGetId(
            [
                'invoice_id'    => $invoice_item_details['invoice_id'], 
                'item_id'       =>  $invoice_item_details['form']['item_id'],
                'title'         =>  $item_details->title, 
                'description'   =>  $description,
                'amount'        =>  $amount, 
                'tax_amount'    =>  $taxable_amount,
                'unit_type'     =>  $item_details->unit_type, 
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s'),
            ]
        );

        //add data to invoice taxes if taxes selected while creating Invoice Items
        if (isset($invoice_item_details['form']['tax']) || isset($invoice_item_details['form']['tax_two'])) {
           
            if (isset($invoice_item_details['form']['tax'])  ) {
                $invoice_taxes_id = DB::table('invoice_taxes')->insertGetId(
                    [
                       
                        'invoice_id'        =>  $invoice_item_details['invoice_id'], 
                        'invoice_item_id'   =>  $invoiceitems,
                        'tax_id'            =>  $tax_one_details->tax_id, 
                        'name'              =>  $tax_one_details->name,
                        'amount'            =>  $tax_one_details->percentage, //tax amount
                        'created_at'        =>  date('Y-m-d H:i:s'),
                        'updated_at'        =>  date('Y-m-d H:i:s'),
                    ]
                );
            }
            if (isset($invoice_item_details['form']['tax_two'])) {
                $invoice_taxes_id = DB::table('invoice_taxes')->insertGetId(
                    [
                        'invoice_id'        =>  $invoice_item_details['invoice_id'], 
                        'invoice_item_id'   =>  $invoiceitems,
                        'tax_id'            =>  $tax_two_details->tax_id, 
                        'name'              =>  $tax_two_details->name,
                        'amount'            =>  $tax_two_details->percentage, //tax amount
                        'created_at'        =>  date('Y-m-d H:i:s'),
                        'updated_at'        =>  date('Y-m-d H:i:s'),
                    ]
                );
            }
            
        }
        if ($invoiceitems) {
            $this->updateInvoiceAmounts($invoice_item_details['invoice_id']);
            $response = array('status' =>'success' ,'message' => 'Item has been added successfully!' );
        }else{
             $response = array('status' =>'error' ,'message' => 'Unable to add item' );
        }
        return $response;
    }
    // This function get the invoice id and update Payable amount and sub total of an Invoice
    public function updateInvoiceAmounts($invoice_id){
        $invoice_details = DB::table('invoices')
        ->where('invoice_id',$invoice_id)->select('invoices.subtotal_amount','invoices.taxable_amount')->first();

        //get the all invoice items for an invoice
        $invoiceitems = DB::table('invoiceitems')
        ->where('invoice_id',$invoice_id)
        ->select('invoiceitems.invoice_item_id','invoiceitems.invoice_id','invoiceitems.item_id','invoiceitems.title',
            'invoiceitems.description','invoiceitems.amount','invoiceitems.tax_amount', DB::raw('amount + tax_amount AS Total')
        )->get();

        $subtotal_amount = 0; $grand_total = 0; $subtotal_amount = 0;
        foreach ($invoiceitems as $key => $value) {
           $subtotal_amount += $value->amount + $value->tax_amount;
           $grand_total     += $value->amount + $value->tax_amount;
        }
        $total_payment_due  = $grand_total + $invoice_details->taxable_amount;
        $update_amounts     = array('payable_amount' =>$total_payment_due , 'subtotal_amount' =>$subtotal_amount);
        //ready for update invoice
        DB::table('invoices')->where('invoice_id', $invoice_id)->update($update_amounts);
        return true;
    }
    //This function helps to view invocies details page.
    public function viewInvocie($invoice_id)
    {
        return view('invoice.view')->with('currency',Config::get('app.currency'));
    }
    //This function returns all the details which need to show on invoice details page
    public function getInvoiceProcessDetails($invoice_id){
        $invoice_details = DB::table('invoices')
        ->where('invoice_id',$invoice_id)
        ->select(
            'invoices.invoice_id','invoices.property_id','invoices.unit_id', 'invoices.sender_id', 'invoices.bill_date', 'invoices.due_date', 'invoices.note', 'invoices.last_email_sent_date',
            'invoices.status', 'invoices.recurring','invoices.recurring_invoice_id','invoices.repeat_every','invoices.repeat_type','invoices.no_of_cycles','invoices.next_recurring_date','invoices.no_of_cycles_completed',
            'invoices.due_reminder_date','invoices.recurring_reminder_date','invoices.discount_amount','invoices.cancelled_at',
            'invoices.cancelled_by', 'invoices.deleted','invoices.subtotal_amount','invoices.taxable_amount','invoices.payable_amount','invoices.is_rental'
        )->first();

        $invoiceitems = DB::table('invoiceitems')
        ->where('invoice_id',$invoice_id)
        ->select('invoiceitems.invoice_item_id','invoiceitems.invoice_id','invoiceitems.item_id','invoiceitems.title',
            'invoiceitems.description','invoiceitems.amount','invoiceitems.tax_amount', DB::raw('amount + tax_amount AS Total')
        )->get();

        $property = DB::table('properties')
        ->where('id',$invoice_details->property_id)
        ->select('properties.id AS property_id','properties.name')->first();

        $units = DB::table('units')
        ->where('id',$invoice_details->unit_id)
        ->select('units.id AS unit_id','units.unit_number')->first();

        $tenancies = DB::table('tenancies')
        ->where('unit_id',$units->unit_id)
         ->leftJoin('tenants', 'tenants.id', '=', 'tenancies.tenant_id')
        ->select('tenancies.rental','tenancies.tenant_id AS tenancie_tenant_id','tenants.name','tenants.email','tenants.phone_number')->first();
        //check it has tenancu created or not
        if ($tenancies) {
            $response['is_have_tenancies']  = true;
            $response['tenancies']          = $tenancies;//$tenancies[0];//i assume only one tenancy for one invoice
        }else{
            $response['is_have_tenancies'] = false;
        }
        foreach ($invoice_details as $key => $value) {
            $invoice[$key] =  $value;
        }
        foreach ($property as $key => $value) {
            $invoice_property_arr[$key] =  $value;
        }
        foreach ($units as $key => $value) {
            $invoice_unit_arr[$key] =  $value;
        }
        $response['invoice']            = $invoice;
        $response['invoice_property']   = $invoice_property_arr;
        $response['invoice_unit']       = $invoice_unit_arr;
        $response['invoiceitems']       = $invoiceitems;
        $sub_total                      = 0;
        $grand_total                    = 0;

        foreach ($invoiceitems as $key => $value) {
           $sub_total   += $value->amount;
           $grand_total += $value->amount + $value->tax_amount;
        }
        $response['sub_total'] = $sub_total;

        //add invoice tax with item total
        $invoice_taxable_amount             = $this->getInvoceTaxableAmount($invoice_id);
        $response['invoice_taxable_amount'] = $invoice_taxable_amount;
        $response['invoice_payable_amount'] = $grand_total+$invoice_taxable_amount;
        $response['grand_total']            = $grand_total;
        return $response;

    }
    //This function get the Invoices Taxable amount
    public function getInvoceTaxableAmount($invoice_id){
        $invoice_tableble_amount = DB::table('invoices')->where('invoice_id',$invoice_id)->select('invoices.taxable_amount')->first();
        return $invoice_tableble_amount->taxable_amount;
    }
    //Get the Items for Invoice Item add
    public function getItems(){
        $all_items = DB::table('items')->where('deleted',0)->select('item_id','title', 'description','unit_type','item_code')->get();
        return $all_items;
    }
    public function deactiveSingleItemApi($invoice_id){
        DB::table('invoices')->where('invoice_id', $invoice_id)->update(['deleted' => 1]);
        $response = array('status' => 'success',);
        return $response;
    }
    //Submited invoice data  and cnahge its status to pending
    public function submitInvoiceData($invoice_id)
    {
        DB::table('invoices')->where('invoice_id', $invoice_id)->update(['status' => 'pending']);
        $response = array(
            'status' => 'success',
            'redirect_to' =>'invoices'
        );
        return $response;
    }
    //Restore deleted invoice again
    public function restoreSingleInvoiceApi($invoice_id)
    {
        DB::table('invoices')->where('invoice_id', $invoice_id)->update(['deleted' => 0]);
        $response = array(
            'status' => 'success',
        );
        return $response;
    }
    //permenantly deleted item for an invoice
    public function deleteSingleItemApi($invoice_item_id){
        $deleted_items = DB::table('invoiceitems')->where('invoice_item_id',$invoice_item_id)->delete();
        $invoice_taxes = DB::table('invoice_taxes')->where('invoice_item_id',$invoice_item_id)->delete();
        if ($deleted_items) {
           $response = array('status' =>'success' , 'message' => 'Invoice item deleted successfully');
        }else{
            $response = array('status' =>'success' , 'message' => 'Unable to delete invoice item');
        }
        return $response;
    }
    //function which is used for get tenancies of the invoice unit selected
    public function getInvoiceTenancy($invoice_id){
        $invoice_details = DB::table('invoices')->where('invoice_id',$invoice_id)->select('invoice_id','property_id','unit_id')->first();
        $tenancies = DB::table('tenancies')->where('unit_id', $invoice_details->unit_id)
        ->leftJoin('tenants', 'tenants.id', '=', 'tenancies.tenant_id')
        ->select('tenancies.id As tenancies_id','tenancies.tenancy_date','tenancies.arc_status','tenancies.rental',
            'tenants.name'
        )->get();
        return $tenancies;
    }
    
    // Invoice api filter(Query query)
    private function filterInvoicesApi($query)
    {
        $invoice_id     = request('invoice_id');
        $unit_number    = request('unit_id');
        $bill_date      = request('bill_date');
        $due_date       = request('due_date');
        $status         = request('status');
        
        $sortkey        = request('sortkey');
        $reverse        = request('reverse');

        if($invoice_id) {
            $query = $query->where('invoices.invoice_id', 'LIKE', '%'.$invoice_id.'%');
        }
        if($unit_number) {
            $query = $query->where('invoices.unit_id', 'LIKE', '%'.$unit_id.'%');
        }
        if($sortkey) {
            $query = $query->orderBy($sortkey, $reverse == 'true' ? 'asc' : 'desc');
        }else{
            $query = $query->orderBy('invoices.invoice_id');
        }

        return $query;
    }
}
