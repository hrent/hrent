<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use DB;

class ItemController extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

    // return user index page
    public function index()
    {
        
       
        return view('item.index');
    }

    // retrieve users by given filter
    public function getItemsApi()
    {
        $perpage = request('perpage');

        $data = DB::table('items')
        //->where('status',1)
        ->select(
            'items.item_id','items.title', 'items.description', 'items.unit_type', 'items.item_code','items.status','items.deleted'
        );
        

        // $data = DB::table('users')
        //  ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
        //  ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
        //     ->select(
        //          'roles.id', 'roles.name AS role_name', 'roles.slug','roles.level','roles.status',
        //           'role_user.id', 'role_user.role_id', 'role_user.user_id',
        //         'users.name', 'users.email', 'users.phone_number', 'users.status','users.last_login_at'
        //     );



        $data = $this->filterItemsApi($data);

        if($perpage != 'All') {
            $data = $data->paginate($perpage);
        }else {
            $data = $data->get();
        }
       
       
       
      // $data['user_role_details'] = $data_roles_details;
       
        return $data;
    }
 // get all users api
    public function getAllItemsApi()
    {
         $items = DB::table('items')
        //->where('status',1)
        ->select(
            'items.item_id','items.title', 'items.description', 'items.unit_type', 'items.item_code','items.status','items.deleted'
        )
            ->orderBy('items.title')
            ->get();

        return $items;
    }
     // store or update new individual user
    public function storeUpdateItemApi(Request $request)
    {
        $data = $request->all();
        
      

        $this->validate(request(), [
            'title' => 'required',
            'description' => 'required',
            'unit_type' => 'required',
            'item_code' => 'required',
            
        ]);
        

        $fieldsArr = [
            'title' => request('title'),
            'description' => request('description'),
            'unit_type' => request('unit_type'),
            'item_code' => request('item_code'),
            
        ];

        if(request('item_id')) {

           
            DB::table('items')
            ->where('item_id', request('item_id'))
            ->update($fieldsArr);
           
           
        }else {
              
          Item::create([
                    'title'           => request('title'),
                    'description'     => request('description'),
                    'unit_type'       => request('unit_type'),
                    'item_code'       => request('item_code'),
                    'created_at'      => date('Y-m-d H:i:s'),
                    'updated_at'      => date('Y-m-d H:i:s'),
                ]);
            $last_id = DB::getPDO()->lastInsertId();
           
            $data['item_id'] =  $last_id;
        }
        $data['error'] = 0;
        return $data;
    }

     // delete single user api(int user_id)
    public function deactivateSingleItemApi($item_id)
    {
        
        DB::table('items')
            ->where('item_id', $item_id)
            ->update(['deleted' => 1]);

        $response = array(
                'status' => 'success',
              
        );
       
    }
    public function restoreSingleItemApi($item_id)
    {
        
        DB::table('items')
            ->where('item_id', $item_id)
            ->update(['deleted' => 0]);

        $response = array(
                'status' => 'success',
              
        );
       
    }
     public function changeItemStatus(Request $request)
    {
         $data =$request->all();

        if($data['status'] == 1){ $status = 0;}else{ $status = 1;}
        DB::table('items')
            ->where('item_id', $data['item_id'])
            ->update(['status' => $status]);

        $response = array(
                'status' => 'success',
                'status' =>$status,
              
        );
       
    }

     // users api filter(Query query)
    private function filterItemsApi($query)
    {
        $title = request('title');
        $description = request('description');
        $unit_type = request('unit_type');
        $sortkey = request('sortkey');
        $reverse = request('reverse');

        if($title) {
            $query = $query->where('items.title', 'LIKE', '%'.$title.'%');
        }
        if($description) {
            $query = $query->where('items.description', 'LIKE', '%'.$description.'%');
        }
        if($unit_type) {
            $query = $query->where('items.unit_type', 'LIKE', '%'.$unit_type.'%');
        }
        if($sortkey) {
            $query = $query->orderBy($sortkey, $reverse == 'true' ? 'asc' : 'desc');
        }else{
            $query = $query->orderBy('items.title');
        }

        return $query;
    }

}
