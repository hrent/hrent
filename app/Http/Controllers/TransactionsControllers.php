<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class TransactionsControllers extends Controller
{
    //
     // return checkout index page
     public function index(Request $request)
     {
         return view('transactions.index');
     }
      // retrieve insurances by given filter
    public function getTransactionsApi()
    {
        $perpage = request('perpage');
        //$insurances = DB::table('units')
        $insurances = DB::table('transactions')
        ->leftJoin('happyrent_insurances', 'happyrent_insurances.insurance_transaction_id', '=', 'transactions.insurance_transaction_id')
        ->leftJoin('units','units.id', '=', 'happyrent_insurances.insurance_unit_id')
        ->leftJoin('properties', 'properties.id', '=', 'units.property_id')
      
        
        ->select(
            'transactions.insurance_transaction_id','transactions.insurance_billplz_id','transactions.insurance_plan_id','transactions.insurance_subtotal_amount','transactions.insurance_taxable_amount', 'transactions.insurance_total_amount', 'transactions.insurance_transaction_status','transactions.insurance_payment_by','transactions.insurance_transaction_date',
            
            'happyrent_insurances.insurance_id', 'happyrent_insurances.insurance_unit_id', 'happyrent_insurances.insurance_plan_id', 'happyrent_insurances.insurance_transaction_id', 'happyrent_insurances.insurance_date', 'happyrent_insurances.insurance_status',
            'happyrent_insurances.insurance_doc_path', 'happyrent_insurances.created_at', 'happyrent_insurances.updated_at',
            
            'units.block_number', 'units.unit_number', 'units.address', 'units.remarks AS unit_remarks', 'units.squarefeet', 'units.purchase_price', 'units.bedbath_room', 'units.purchased_at', 'units.id',
        
            'properties.id AS property_id', 'properties.name AS property_name', 'properties.city', 'properties.state',
            'properties.postcode'
        );
        $insurances = $this->filterInsurancesApi($insurances);
        if($perpage != 'All') {
            $insurances = $insurances->paginate($perpage);
        }else {
            $insurances = $insurances->get();
        }
        $insurances_plans = DB::table('happyrent_insurance_plans')
        ->select(
            'happyrent_insurance_plans.insurance_plan_id', 'happyrent_insurance_plans.insurance_plan_name', 'happyrent_insurance_plans.insurance_plan_price', 'happyrent_insurance_plans.insurance_plan_duration','happyrent_insurance_plans.insurance_plan_status'
        )
        ->orderBy('happyrent_insurance_plans.insurance_plan_name')
        ->get();
        $insurances_re['insurances']    = $insurances;
        $insurances_re['plans']         = $insurances_plans;
        return $insurances_re;
    }
    private function filterInsurancesApi($query)
    {
        $block_number = request('block_number');//name
        $unit_number = request('unit_number');//coverages
        $address = request('address');//remarks
        $status = request('status');
        $sortkey = request('sortkey');
        $reverse = request('reverse');

        if($block_number) {
            $query = $query->where('units.block_number', 'LIKE', '%'.$block_number.'%');
        }
        if($unit_number) {
            $query = $query->where('units.unit_number', 'LIKE', '%'.$unit_number.'%');
        }
        if($address) {
            $query = $query->where('units.address', 'LIKE', '%'.$address.'%');
        }
        if($sortkey) {
            $query = $query->orderBy($sortkey, $reverse == 'true' ? 'asc' : 'desc');
        }else{
            $query = $query->orderBy('units.block_number');
        }

        return $query;
    }
}
