<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(Auth::guest()) {
            return redirect('login');
        }else{
            $check_agency = $this->loginUserAgencyDetails($request);
            if (!$check_agency) {
                $request->session()->forget('agency_name');
                $request->session()->forget('agency_id');
            }
            return view('home');
        }
    }

    public function loginUserAgencyDetails($request)
    {
        $agencies_members = DB::table('users')
        ->where('users.id', '=',  Auth::user()->id)
        ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
        ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
        ->where('roles.slug', '=', 'agency')
        ->select(
            'users.id AS member_id',
            'role_user.user_id',
            'roles.id AS role_id', 'roles.name AS role_name', 'roles.slug','roles.level'
        )->first();
       
        if ($agencies_members) {
            $agency_details = DB::table('users')
            ->where('users.id', '=',  Auth::user()->id)
            ->leftJoin('agency_admin_role_relation', 'agency_admin_role_relation.user_id', '=', 'users.id')
            ->leftJoin('agencies', 'agencies.id', '=', 'agency_admin_role_relation.agency_id')
            ->select(
               'agencies.id AS agency_id', 'agencies.name AS agency_name', 'agencies.logo'
            )->get();
            $request->session()->put('agency_name',$agency_details[0]->agency_name);
            $request->session()->put('agency_id',$agency_details[0]->agency_id);
            return true;
        }else{
            $request->session()->forget('agency_name');
            $request->session()->forget('agency_id');
            return false;
        }
    }
}
