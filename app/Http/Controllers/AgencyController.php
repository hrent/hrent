<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use URL;
use App\Agency;

class AgencyController extends Controller
{
    // auth access
    public function __construct()
    {
        $this->middleware('auth');
    }

    // return user index page
    public function index()
    {   return view('agency.index');
    }
   // retrieve users by given filter
    public function getAllAgencies()
    {
        $perpage = request('perpage');
        $agencies = DB::table('agencies')
        ->select(
            'agencies.id','agencies.name','agencies.logo', 'agencies.address1','agencies.address2','agencies.location','agencies.latitude','agencies.longitude','agencies.country_id','agencies.state_id','agencies.city_id','agencies.user_id','agencies.is_active','agencies.created_at','agencies.updated_at', 'agencies.prefix', 'agencies.tenancy_running_num'
        );

        $agencies = $this->filterAgenciesApi($agencies);

        if($perpage != 'All') {
            $agencies = $agencies->paginate($perpage);
        }else {
            $agencies = $agencies->get();
        }
        $agencies_response['agencies'] = $agencies;

        $agencies_response['base_url'] = URL::to('/');
       	return $agencies_response;
    }
    public function getBaseUrl(){
         $base_url= URL::to('/');
        return $base_url;
    }
    public function getAllAgenciesMembers()
    {
        $agencies_members = DB::table('agency_admin_role_relation')

        ->where('agency_admin_role_relation.status', '=', 1)
        ->select(
            'agency_admin_role_relation.user_id')->get();

        foreach ($agencies_members as $key => $value) {
            $existing_memebr_ids[] = $value->user_id;
        }

       if (!empty($existing_memebr_ids)) {
           $agencies_members_list = DB::table('users')
            ->whereNotIn('user_id',$existing_memebr_ids)
            ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.slug', '=', 'agency')
            ->select(
                'users.id AS member_id','users.name', 'users.email', 'users.phone_number', 'users.status','users.last_login_at','users.deleted','users.idtype_id','users.id_value',
                'role_user.user_id',
                'roles.id AS role_id', 'roles.name AS role_name', 'roles.slug','roles.level'
            )->get();
       }else{
            $agencies_members_list = DB::table('users')

            ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.slug', '=', 'agency')
            ->select(
                'users.id AS member_id','users.name', 'users.email', 'users.phone_number', 'users.status','users.last_login_at','users.deleted','users.idtype_id','users.id_value',
                'role_user.user_id',
                'roles.id AS role_id', 'roles.name AS role_name', 'roles.slug','roles.level'
            )->get();
       }


        return $agencies_members_list;
    }
    // delete single agency api(int agency_id)
    public function deactivateSingleAgencyApi($agency_id)
    {
        DB::table('agencies')
        ->where('id', $agency_id)
        ->update(['is_active' => 0]);
        $response = array(
           'status' => 'success',
        );

    }
    public function restoreSingleAgencyApi($tax_id)
    {
        DB::table('agencies')
        ->where('id', $tax_id)
        ->update(['is_active' => 1]);
        $response = array(
            'status' => 'success',
            'message' => 'Deleted Successfully',
        );

    }
    public function changeAgencyStatus(Request $request)
    {
        $data =$request->all();
        if($data['status'] == 1){ $status = 0;}else{ $status = 1;}
        DB::table('agencies')
        ->where('id', $data['agency_id'])
        ->update(['is_active' => $status]);
        $response = array(
            'status' => 'success',
            'message' => 'Status chnaged Successfully',
        );
       return $response;
    }
    public function addMemberToAgency(Request $request)
    {
        $member_agency_details = $request->all();
         $this->validate(request(), [
            'member_id' => 'required',
            'id' => 'required',
        ]);
         if (empty( $member_agency_details['member_id'])) {
            $response = array(
                'status' => 'error',
                'message' =>'Unable to add this member to '.$member_agency_details['name']
            );
         }else{
            $agency_admin_role_relation = DB::table('agency_admin_role_relation')->insert([
            'user_id'          => $member_agency_details['member_id'],
            'agency_id'        => $member_agency_details['id'],
            'created_at'       => date('Y-m-d H:i:s'),
            'updated_at'       => date('Y-m-d H:i:s'),

            ]);
            $response = array(
                'status' => 'success',
                'message' =>'Successfully added this member to '.$member_agency_details['name']
            );
         }
        return $response;
    }
    // Agency api filter(Query query)
    private function filterAgenciesApi($query)
    {
        $id = request('id');
        $name = request('name');
        $address1 = request('address1');
        $created_at = request('created_at');
        $is_active = request('is_active');

        $sortkey = request('sortkey');
        $reverse = request('reverse');

        if($id) {
            $query = $query->where('agencies.id', 'LIKE', '%'.$id.'%');
        }
        if($name) {
            $query = $query->where('agencies.name', 'LIKE', '%'.$name.'%');
        }
        if($address1) {
            $query = $query->where('agencies.address1', 'LIKE', '%'.$address1.'%');
        }
         if($created_at) {
            $query = $query->where('agencies.created_at', 'LIKE', '%'.$created_at.'%');
        }
        if($is_active) {
            $query = $query->where('agencies.is_active', 'LIKE', '%'.$is_active.'%');
        }

        if($sortkey) {
            $query = $query->orderBy($sortkey, $reverse == 'true' ? 'asc' : 'desc');
        }else{
            $query = $query->orderBy('agencies.id');
        }

        return $query;
    }
    public function storeUpdateAgencyApi(Request $request){
        $data  = Input::all();

        if (isset($data['agency_details']['address2'])) {
    		$address2 = $data['agency_details']['address2'];
    	}else{
    		$address2 = '';
    	}
    	if (isset($data['agency_details']['latitude'])) {
    		$latitude = $data['agency_details']['latitude'];
    	}else{
    		$latitude = '';
    	}
    	if (isset($data['agency_details']['longitude'])) {
    		$longitude = $data['agency_details']['longitude'];
    	}else{
    		$longitude = '';
    	}
        if(isset($data['agency_details']['id'])) {
            $fieldsArr = [
                'name'             => $data['agency_details']['name'],
                'address1'         => $data['agency_details']['address1'],
                'address2'         => $address2,
                'location'         => $data['agency_details']['location'],
                'latitude'         => $latitude,
                'longitude'        => $longitude,
                'country_id'       => $data['agency_details']['country_id'],
                'state_id'         => $data['agency_details']['state_id'],
                'city_id'          => $data['agency_details']['city_id'],
                'prefix'           => $data['agency_details']['prefix']
            ];

            $agency = Agency::findOrFail($data['agency_details']['id']);
            $agency->update($fieldsArr);

        }else{
            $agency = DB::table('agencies')->create([
                'name'             => $data['agency_details']['name'],
                'user_id'          => Auth::user()->id,
                'address1'         => $data['agency_details']['address1'],
                'address2'         => $address2,
                'location'         => $data['agency_details']['location'],
                'latitude'         => $latitude,
                'longitude'        => $longitude,
                'country_id'       => $data['agency_details']['country_id'],
                'state_id'         => $data['agency_details']['state_id'],
                'city_id'          => $data['agency_details']['city_id'],
                'prefix'           => $data['agency_details']['prefix']

            ]);
        }

        if(isset($data['image'])) {
            $agency_id = $agency->id;
            $png_url = "agency_" . $agency_id . '_logo_' . time() . ".png";

            $path = public_path('agency');
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }

            $path = public_path() . '/agency/' . $png_url;
            Image::make(file_get_contents($data['image']))->save($path);

            $agency_logo_path = '/agency/' . $png_url;
            //update path
            $agency->update(['logo' => $agency_logo_path]);
        }

        $response = array(
            'status' => 'success',
            'agency_id' =>'Details has been update successfully!'
        );
        return $response;
    }
}
