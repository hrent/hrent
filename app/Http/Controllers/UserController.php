<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\FilterPhoneNumber;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use File;
class UserController extends Controller
{
    use FilterPhoneNumber;
    // auth access
    public function __construct()
    {
        $this->middleware('auth');
    }

    // return user index page
    public function index()
    {
        return view('user.index');
    }

    // get all users api
    public function getAllUsersApi()
    {
        $users = DB::table('users')
        ->select(
            'users.name', 'users.email', 'users.phone_number', 'users.status','users.deleted'
        )
        ->orderBy('users.name')
        ->get();
        return $users;
    }



    // retrieve users by given filter
    public function getUsersApi()
    {
        $perpage = request('perpage');

        $data = User::leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
        ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
        ->select(
            'users.id','users.name', 'users.email', 'users.phone_number', 'users.status','users.last_login_at','users.deleted','users.idtype_id','users.id_value',
            'role_user.user_id',
            'roles.id AS role_id', 'roles.name AS role_name', 'roles.slug','roles.level'
        )->distinct();
        $data = $this->filterUsersApi($data);

        if($perpage != 'All') {
            $data = $data->paginate($perpage);
        }else {
            $data = $data->get();
        }
        return $data;
    }

    public function getusersroles(Request $request){

        $data = $request->all();



        $data_roles_details = DB::table('role_user')
        ->where('user_id', $data['user_id'])
        ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
        ->select(
            'role_user.user_id',
            'roles.id', 'roles.name AS role_name', 'roles.slug','roles.level','roles.status'
        )->get();
        return $data_roles_details;

    }

    // store or update new individual user
    public function storeUpdateUserApi(Request $request)
    {
        $data = $request->all();
       //print_r($data);die;
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
        ]);
        $fieldsArr = [
            'name' => request('name'),
            'email' => request('email'),
            'phone_number' => $this->filterMalaysiaPhoneNumber(request('phone_number')),
            'password' => request('password'),
            'idtype_id' => request('idtype_id'),
            'id_value' => request('id_value'),
            'profile_id' => auth()->user()->profile_id
        ];

       $get_roles = DB::table('roles')
        ->where('slug',request('slug'))
        ->select(
                'roles.id','roles.name', 'roles.slug','roles.status'
        )->first();


        if(request('id')) {
            if(request()->has('password')) {
                $fieldsArr['password'] = request('password');
            }
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|unique:users,email,'.request('id'),
                'phone_number' => 'required|unique:users,phone_number,'.request('id'),
                'password' => 'confirmed'
            ]);

            $data = User::findOrFail(request('id'));
            $data->update($fieldsArr);

            //update role (This will be changed for multiassing roles)
            DB::table('role_user')
            ->where('user_id', request('id'))
            ->update(['role_id' =>$get_roles->id]);

        }else {
/*
            $fieldsArr['password'] = request('password');
            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|unique:users',
                'phone_number' => 'required|unique:users',
                'password' => 'required|confirmed'
            ]);
            User::create([$fieldsArr]); */

            User::create([
                    'name' => request('name'),
                    'email' => request('email'),
                    'idtype_id' => request('idtype_id'),
                    'id_value' => request('id_value'),
                    'password' => request('password'),
                    'phone_number' =>  request('phone_number'),//$this->filterMalaysiaPhoneNumber(request('phone_number'))
                ]);
            $last_id = DB::getPDO()->lastInsertId();



            DB::table('role_user')->insert([
                'role_id'                   => $get_roles->id,
                'user_id'                   => $last_id,
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),

            ]);
            $data['user_id']  = $last_id;
            $data['userrole'] = request('role_id');

        }
        $data['error'] = 0;
        return $data;
    }

    public function checkUserExistByEmail($email){
        $user = User::where('email', '=', $email)->first();
        if ($user === null) {
           // user doesn't exist
            $user= true;
        }else{
            $user= false;
        }
    }
    public function checkUserExistByPhoneNumber($phone){
        $user = User::where('phone_number', '=', $phone)->first();
        if ($user === null) {
           // user doesn't exist
             $user= true;
        }else{
             $user= false;
        }
    }
    public function storeExistUpdateUserApi(Request $request)
    {
        $user = array();
        $data = $request->all();
        if (isset($data)) {
            if (isset($data['email'])) {

                $user = User::where('email', '=', $data['email'])->first();

            }
            if (isset($data['phone_number'])) {
               $user = User::where('phone_number', '=', $data['phone_number'])->first();
            }

            if ($user) {
               $user_assigned_roles = DB::table('role_user')
                ->where('user_id', $user->id)
                ->select('id','role_id')->get();

                //print_r($user_assigned_roles);die;
                foreach ($user_assigned_roles as $assig) {
                    $assigroles[] = $assig->role_id;

                }

                 $user['newroles'] = DB::table('roles')
                    ->whereNotIn ('id', $assigroles)
                    ->select(
                        'roles.id','roles.name', 'roles.slug', 'roles.description', 'roles.level','roles.status'
                    )
                    ->orderBy('roles.name')
                    ->get();


                $user['error'] = 0;
            }else{
                $user['error'] = 'Email OR Phone number dose not exist.';
            }


        }else{
            $user['error'] = 'Please enter Phone Number OR Email';
        }

        return $user;
    }

    public function addMoreRole(Request $request) {
        $data = $request->all();
        DB::table('role_user')->insert([
                'role_id'                   => $data['role_id'],
                'user_id'                   => $data['user_id'],
                'created_at'                => date('Y-m-d H:i:s'),
                'updated_at'                => date('Y-m-d H:i:s'),
            ]);
        return $data;
    }


    // delete single user api(int user_id)
    public function deactivateSingleUserApi($user_id)
    {
        // $data = User::findOrFail($user_id);
        // $data->status = 0;
        // $data->save;
        DB::table('users')
            ->where('id', $user_id)
            ->update(['deleted' => 1]);

        $response = array(
                'status' => 'success',

        );
       return  $response;
    }
    public function restoreSingleUserApi($user_id)
    {

        DB::table('users')
            ->where('id', $user_id)
            ->update(['deleted' => 0]);

        $response = array(
                'status' => 'success',

        );
       return  $response;
    }
     public function changeStatus(Request $request)
    {
         $data =$request->all();
        if($data['status'] == 1){ $status = 0;}else{ $status = 1;}
        DB::table('users')
            ->where('id', $data['user_id'])
            ->update(['status' => $status]);

        $response = array(
                'message' => 'success',
                'status' =>$status,

        );
        return $response;

    }



    // return self user account index($user_id)
    public function userAccountIndex($user_id)
    {
        return view('user.account', compact('user_id'));
    }

    // retrieve single user api (int user_id)
    public function getSingleUserApi($user_id)
    {
        $user = User::findOrFail($user_id);

        return $user;
    }

    // users api filter(Query query)
    private function filterUsersApi($query)
    {
        $name = request('name');
        $phone_number = request('phone_number');
        $email = request('email');
        $sortkey = request('sortkey');
        $reverse = request('reverse');

        if($name) {
            $query = $query->where('users.name', 'LIKE', '%'.$name.'%');
        }
        if($phone_number) {
            $query = $query->where('users.phone_number', 'LIKE', '%'.$phone_number.'%');
        }
        if($email) {
            $query = $query->where('users.email', 'LIKE', '%'.$email.'%');
        }
        if($sortkey) {
            $query = $query->orderBy($sortkey, $reverse == 'true' ? 'asc' : 'desc');
        }else{
            $query = $query->orderBy('users.name');
        }

        return $query;
    }
    public function getRolesApi()
    {
          if(Auth::check() && Auth::user()->hasRole('admin', true)){

            $roles = DB::table('roles')
            ->whereIn('slug',['admin','agency'])
            ->select(
                'roles.id','roles.name', 'roles.slug', 'roles.description', 'roles.level','roles.status'
            )
            ->orderBy('roles.name')
            ->get();
        }else{
             $roles = DB::table('roles')
            ->select(
                'roles.id','roles.name', 'roles.slug', 'roles.description', 'roles.level','roles.status'
            )
            ->orderBy('roles.name')
            ->get();
        }


        return $roles;
    }


    public function getCountries(){
         $countries = DB::table('countries')
                ->whereIn('code', ['MY', 'SG', 'IN'])
                ->select('id','code','name','phonecode')->get();

        return $countries;
    }
     public function getStates(Request $request){
        $county = $request->all();
        $states = DB::table('states')
                ->where('country_id', $county['country_id'])
                ->select('id','name')->get();

        return $states;
    }
    public function getCities(Request $request){
        $state = $request->all();
        $cities = DB::table('cities')
                ->where('state_id', $state['state_id'])
                ->select('id','name')->get();

        return $cities;
    }
    public function getAssignRoles($user_id){
        $role_name = '';
        $roles = DB::table('role_user')
        ->where('user_id',$user_id)
        ->select(
            'role_user.id', 'role_user.role_id', 'role_user.user_id'
        )->get();


        if ($roles) {
            foreach ($roles as $key => $value) {
               $ids[] = $value->role_id;
            }
            $roles_details = DB::table('roles')
            ->whereIn('id',$ids)
            ->select(
                    'roles.id', 'roles.name', 'roles.slug','roles.level','roles.status'
            )->get();

            $i=0;
            foreach ($roles_details as $role_key => $role_details) {
                $i++;
                if ($i > 1) {
                    $role_name .= ', '.$role_details->name;
                }else{
                    $role_name .= ' '.$role_details->name;
                }

            }
        }
        return $role_name;
    }
}
