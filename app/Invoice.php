<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /*

    type
    <option value="1">ARC Rental</option>
    <option value="2">ARC Others</option>
    <option value="3">Utilities - Electric</option>
    <option value="4">Utilities - Water</option>
    <option value="5">Utilities - Others</option>
    <option value="6">Top Up</option>
    <option value="7">Insurance</option>
    <option value="8">MC & SF</option>
    <option value="9">Others</option>

    status
    1 = open
    2 = draft
    3 = confirmed
    99 = archieved

    payment_status
    1 = owe
    2 = paid
    */

    protected $fillable = [
        'property_id','unit_id', 'tenancy_id','sender_id', 'bill_date', 'due_date', 'note', 'last_email_sent_date',
        'status', 'recurring','recurring_invoice_id','repeat_every','repeat_type','no_of_cycles','next_recurring_date','no_of_cycles_completed',
        'due_reminder_date','recurring_reminder_date','discount_amount','discount_amount_type','discount_type','cancelled_at',
        'cancelled_by', 'deleted','coupons_id','subtotal_amount','taxable_amount','payable_amount'
    ];
}
