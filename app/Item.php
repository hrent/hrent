<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'item_id','title','description','unit_type','item_code','status','deleted',
    ];
}
