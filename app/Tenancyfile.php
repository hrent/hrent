<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenancyfile extends Model
{
    protected $fillable = [
        'name', 'url',
        'tenancy_id'
    ];
}
