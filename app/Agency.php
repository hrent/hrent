<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $fillable = [
        'id','name','logo', 'address1','address2',
        'location','latitude','longitude','country_id','state_id',
        'city_id','user_id','is_active','created_at','updated_at',
        'prefix', 'tenancy_running_number'
    ];
}
