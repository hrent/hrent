<template id="form-tax-template">
  <div class="modal" id="tax_modal">
      <div class="modal-dialog modal-lg">
          <form action="#" @submit.prevent="onSubmit" method="POST" autocomplete="off">
          <div class="modal-content">
              <div class="modal-header back-happyrent-light-green text-white">
                  <div class="modal-title">
                      @{{form.tax_id ? 'Edit Tax' : 'Add new Tax'}}
                  </div>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                
                 <!-- Name and Logo section -->
                        <div class="row">
                            <div class="form-group col">
                               <label class="control-label">
                                Name
                            </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                     
                                    <input type="text" name="name" class="form-control" v-model="form.name" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-money-bill-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col">
                                <label class="control-label">
                                    Percentage
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                  <div class="input-group">
                                     
                                     <input type="text" name="percentage" class="form-control" v-model="form.percentage" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-percentage"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
              </div>
              <div class="modal-footer">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-success" v-if="!form.tax_id">Create</button>
                    <button type="submit" class="btn btn-success" v-if="form.tax_id">Update</button>
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                  </div>
              </div>
              </form>
          </div>
      </div>
  </div>
</template>