<template id="form-agency-template">
  <div class="modal" id="agency_modal">
      <div class="modal-dialog modal-lg">
            <form action="#" @submit.prevent="onMemberSubmit" method="POST" autocomplete="off">
                <div class="modal-content">
                    <div class="modal-header back-happyrent-light-green text-white">
                        <div class="modal-title">
                            @{{form.id ? 'Edit Agency' : 'Add New Agency'}}
                        </div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                   <div class="modal-body">
                        <!-- Name and Logo section -->
                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Name
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <input type="text"  name="name" class="form-control" v-model="form.name" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="far fa-address-card"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Prefix
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <input type="text"  name="prefix" class="form-control" v-model="form.prefix" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-font"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col">
                               <label class="control-label">
                                    Logo
                                </label> <label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group" v-if="!form.id">
                                     <input type="file" name="image" class="form-control" @change="imagePreview($event)" >
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="far fa-image"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="input-group" v-if="form.id">
                                    <img v-bind:src="base_url+form.logo" style="    margin-top: -46px;width: 150; height:100;margin-left: 35%;" />
                                </div>
                            </div>
                        </div>

                        <!-- address1 and address2 section -->
                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Address1
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <input type="text" name="address1" class="form-control" v-model="form.address1" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-map-marked-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group col">
                                <label class="control-label">
                                    Address2
                                </label>
                                <div class="input-group">
                                    <input type="text" name="address2" class="form-control" v-model="form.address2" >
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-map-marked-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- country and state section -->
                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Country
                                </label> <label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <select  name="country_id" @change="getState($event)" class="form-control" v-model="form.country_id" required>
                                        <option  v-for="(country,index)  in countries" v-bind:value="country.id">@{{ country.name }}</option>
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-globe-asia"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group col">
                                 <label class="control-label">
                                    State
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <select  name="state_id" @change="getCity($event)" class="form-control" v-model="form.state_id" required>
                                        <option  v-for="(state,index)  in states" v-bind:value="state.id">@{{ state.name }}</option>
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-globe-asia"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                       <!-- country and state section -->
                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    City 
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <select name="city_id" class="form-control" v-model="form.city_id" required>
                                        <option  v-for="(city,index)  in cities" v-bind:value="city.id">@{{ city.name }}</option>
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="fas fa-city"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col">
                                <label class="control-label">
                                    Location
                                </label><label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <input type="text" name="location" class="form-control" v-model="form.location" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-location-arrow"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- city and location-->
                        <!-- Lat and Long section -->
                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                Latitude
                                </label>
                                <div class="input-group">
                                    <input type="text" name="latitude" class="form-control" v-model="form.latitude">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-map-marker-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                             <div class="form-group col">
                                <label class="control-label">
                                    Longitude
                                </label>
                                <div class="input-group">
                                     <input type="text" name="longitude" class="form-control" v-model="form.longitude">
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                           <i class="fas fa-map-marker-alt"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                      <div class="btn-group">
                        <button type="submit" class="btn btn-success" v-if="!form.id">Create</button>
                        <button type="submit" class="btn btn-success" v-if="form.id">Update</button>
                        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                </form>
          </div>
      </div>
  </div>
</template>