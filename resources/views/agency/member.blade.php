<template id="agency-member-template">
  <div class="modal" id="agency_member_modal">
      <div class="modal-dialog modal-lg">
            <form action="#" @submit.prevent="onMemberSubmit()" method="POST" autocomplete="off">
                <div class="modal-content">
                    <div class="modal-header back-happyrent-light-green text-white">
                        <div class="modal-title">
                            Add New Member to : <b> @{{form.name}} </b>
                        </div>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                   <div class="modal-body">
                        <!-- Name and Logo section -->
                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Member
                                </label> <label for="required" class="control-label" style="color:red;">*</label>
                                <div class="input-group">
                                    <select  name="member_id"  class="form-control" v-model="form.member_id" required>

                                        <option  v-for="(member,index)  in members" :value="member.member_id">@{{ member.name }}</option>
                                    </select>
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                             <i class="fas fa-user-plus"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col">
                               <label class="control-label">
                                    &nbsp;
                                </label> 
                                <div class="input-group">
                                    <label for="required" class="control-label" ><i style="margin-top: 10px;" class="fa fa-question-circle" v-b-tooltip.hover title="Member will be able to access this agency details after login!"></i>   </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <div class="btn-group">
                        <button type="submit" class="btn btn-success" >Add</button>
                        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    
                </form>
          </div>
      </div>
  </div>
</template>