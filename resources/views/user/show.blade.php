<template id="show-user-template">
    <div class="modal" id="show_user_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header back-happyrent-light-green text-white">
                    <div class="modal-title" >
                       Change Password  @{{form.id}}
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    
                   @{{data.name}}
                    
                </div><!--modal-body -->
            </div><!-- modal content-->
        </div>
    </div>
</template>