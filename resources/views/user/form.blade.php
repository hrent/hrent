<template id="form-user-template">
    <div class="modal" id="user_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header back-happyrent-light-green text-white">
                    <div class="modal-title" >
                        @{{form.id ? 'Edit User' : 'Add New User'}}
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <b-tabs content-class="mt-3" v-if="existingSubmit == false">
                        <b-tab title="User Details" class="new_user" active>

                            <form action="#" @submit.prevent="onSubmit" method="POST" autocomplete="off">

                                <div class="row">
                                    <div class="form-group col">
                                         <label class="control-label">
                                                Name
                                            </label> <label for="required" class="control-label" style="color:red;">*</label>
                                        <div class="input-group">
                                           <input type="text" name="name" class="form-control" v-model="form.name" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                           <i class="far fa-keyboard"></i>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                     <div class="form-group col">
                                         <label class="control-label">
                                                Role
                                            </label> <label for="required" class="control-label" style="color:red;">*</label>
                                        <div class="input-group">
                                        <!--   <select  name="role_id" class="form-control" v-model="form.role_id" required>
                                                <option  v-for="(role,index)  in userroles" v-bind:value="role.slug" :selected="role.id == form.role_id">@{{ role.name }}</option>
                                            </select> -->

                                              <select2 name="slug"  v-model="form.slug">
                                                <option value="">All</option>
                                                <option v-for="(role,index)  in userroles" :value="role.slug">
                                               @{{ role.name }}
                                            </select2>

                                        </div>
                                    </div>
                                </div>

                                <!-- Email and Phone number-->
                                 <div class="row">
                                    <div class="form-group col">
                                         <label class="control-label">
                                      Email
                                    </label> <label for="required" class="control-label" style="color:red;">*</label>
                                        <div class="input-group">
                                          <input type="email" name="email" class="form-control" v-model="form.email" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="far fa-envelope"></i>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                     <div class="form-group col">
                                          <label class="control-label">
                                        Phone Number <i class="fa fa-question-circle" v-b-tooltip.hover title="Add numer with country code +60 +65 +91"></i>
                                    </label>
                                        <div class="input-group">
                                           <input type="text" name="phone_number" class="form-control" v-model="form.phone_number" placeholder="Phone with code +60 +65 +91  " required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="fas fa-mobile-alt"></i>
                                            </span>

                                        </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Password and confirm password-->
                                  <div class="row" v-if="!form.id">
                                    <div class="form-group col">
                                          <label class="control-label">
                                                Password
                                            </label> <label for="required" class="control-label" style="color:red;">*</label>
                                        <div class="input-group">
                                         <input type="password" name="password" class="form-control" v-model="form.password" autocomplete="off"  required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="fas fa-lock"></i>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                     <div class="form-group col">
                                            <label class="control-label">
                                                Password Confirmation
                                            </label>
                                        <div class="input-group">
                                          <input type="password" name="password_confirmation" class="form-control" autocomplete="off" v-model="form.password_confirmation"  required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                           <i class="fas fa-lock"></i>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Id Type -->
                                  <div class="row" >
                                    <div class="form-group col">
                                         <label class="control-label">
                                            ID Type
                                        </label>
                                        <label for="required" class="control-label" style="color:red;">*</label>
                                        <div class="input-group">

                                            <select2 name="idtype_id"  v-model="form.idtype_id">

                                                <option v-for="idtype in idtype_options" :value="idtype.id">
                                              @{{idtype.name}}
                                               </option>
                                            </select2>


                                        <div class="input-group-append">

                                        </div>
                                        </div>
                                    </div>
                                     <div class="form-group col" v-if="form.idtype_id">
                                            <label class="control-label">
                                        @{{idtype_options[form.idtype_id - 1].name}} Number
                                    </label><label for="required" class="control-label" style="color:red;">*</label>
                                        <div class="input-group">
                                        <input type="text" name="id_value" class="form-control" v-model="form.id_value" required>
                                        <div class="input-group-append">

                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group col" v-if="!form.idtype_id">
                                            <label class="control-label">
                                                &nbsp;
                                        </label>

                                    </div>
                                </div>
                                <div class="float-right">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-success" v-if="!form.id">Create</button>
                                        <button type="submit" class="btn btn-success" v-if="form.id">Save</button>
                                        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </form>
                        </b-tab>
                        <b-tab title="Existing User" v-if="!form.id" class="existing_user">
                            <form action="#" @submit.prevent="onExistingSubmit" method="POST" autocomplete="off">
                               <!-- Email and Phone number-->
                                 <div class="row">
                                    <div class="form-group col">
                                         <label class="control-label">
                                      Email
                                    </label>
                                        <div class="input-group">
                                         <input type="email" name="email" class="form-control" v-model="exeform.email" >
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="far fa-envelope"></i>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                    (OR)
                                     <div class="form-group col">
                                          <label class="control-label">
                                        Phone Number <i class="fa fa-question-circle" v-b-tooltip.hover title="Add numer with country code +60 +65 +91"></i>
                                    </label>
                                        <div class="input-group">
                                           <input type="text" name="phone_number" class="form-control" v-model="exeform.phone_number" placeholder="Phone with code +60 +65 +91  " >
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                            <i class="fas fa-mobile-alt"></i>
                                            </span>

                                        </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="btn-group" style="margin-left: 80%">
                                    <button type="submit"  class="btn btn-success">Create</button>


                                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                                </div>
                            </form>

                        </b-tab>

                    </b-tabs>

                    <form action="#"  v-if="existingSubmit == true" @submit.prevent="existingUserNewRoleSubmit" method="POST" autocomplete="off">
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">
                                Name :  @{{existinguser.name}}
                            </label>
                        </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">
                                Email: @{{existinguser.email}}
                            </label>

                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">
                                Role
                            </label> <label for="required" class="control-label" style="color:red;">*</label>
                            <select  name="role_id" class="form-control" v-model="exeroleform.role_id" required>
                                <option  v-for="(role,index)  in filteredroles"  v-bind:value="role.id">@{{ role.name }}</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-success">Create</button>
                                <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>



                </div><!--modal-body -->
            </div><!-- modal content-->
        </div>
    </div>
</template>