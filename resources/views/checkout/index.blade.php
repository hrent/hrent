@extends('layouts.app')
@section('header')
    <a href="/insurance" class="btn btn-sm back-happyrent-light-green">
        <i class="fas fa-house-damage"></i>
        checkout
    </a>
@stop
@section('content')
<div>
    <div class="panel panel-default">
        <div class="panel-body screen-panel">
            <div id="indexCheckoutController">
                <index-checkout></index-checkout>
            </div>
        </div>
    </div>
</div>
<template id="index-checkout-template">
    <div>
        <div class="card">
            <div class="card-header text-white">
                <div class="form-row">
                    <span class="mr-auto">
                      <i class="fas fa-house-damage"></i>
                      Checkout
                    </span>
                </div>
            </div>
            <div class="card-body">
                <div class="form-row" style="padding-top: 20px;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr class="table-secondary">
                                <th class="text-center">
                                    <a href="#" @click="sortBy('block_number')">Block Number</a>
                                    <span v-if="sortkey == 'block_number' && !reverse" class="fa fa-caret-down"></span>
                                    <span v-if="sortkey == 'block_number' && reverse" class="fa fa-caret-up"></span>
                                </th>
                                <th class="text-center">
                                    <a href="#" @click="sortBy('unit_number')">Unit Number</a>
                                    <span v-if="sortkey == 'unit_number' && !reverse" class="fa fa-caret-down"></span>
                                    <span v-if="sortkey == 'unit_number' && reverse" class="fa fa-caret-up"></span>
                                </th>
                                <th class="text-center">
                                    <a href="#" @click="sortBy('property_id')">Property</a>
                                    <span v-if="sortkey == 'property_id' && !reverse" class="fa fa-caret-down"></span>
                                    <span v-if="sortkey == 'property_id' && reverse" class="fa fa-caret-up"></span>
                                </th>
                                <th class="text-center">
                                    <a href="#" @click="sortBy('insurance_status')">Plan</a>
                                    <span v-if="sortkey == 'insurance_status' && !reverse" class="fa fa-caret-down"></span>
                                    <span v-if="sortkey == 'insurance_status' && reverse" class="fa fa-caret-up"></span>
                                </th>
                                <th class="text-center">
                                    Amount
                                </th>
                                <th></th>
                            </tr>
                            <tr v-for="(data, index) in list" class="row_edit">
                                <td class="text-center">
                                    @{{ data.block_number }}
                                </td>
                                <td class="text-center">
                                    @{{ data.unit_number }}
                                </td>
                                <td class="text-center">
                                    @{{ data.address }}
                                </td>
                                <td class="text-center"  >
                                    <p v-for="(plan,yindex) in plans" v-if="yindex==index"> @{{ plan.insurance_plan_name }} @{{ plan.insurance_plan_price }}</p>
                                </td>
                                <td class="text-center">
                                    <p v-for="(plan,yindex) in plans" v-if="yindex==index"> @{{ plan.insurance_plan_price }}</p>
                                </td>
                                <td>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-danger btn-sm" @click="removeSingleUnit(data)">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div> <!-- /table-responsive -->
                    <div class="pull-left">
                        <p><b>Tax Amount:  @{{tax_amount}}  RM</b> </p>
                        <p><b>Sub Total:  @{{total_amount}}  RM</b> </p>
                        <p><b>Total Amount:  @{{total_amount}}  RM</b></p>
                       
                        <b-button type="button" id="createinsurance"   @click="createBillForInsurance"  variant="primary" class="m-1"  v-bind:disabled="isContinueButtonDisabled" style="width: 100%;">
                                Continue
                        </b-button>
                       
                      
                    </div>
                   
                </div><!-- /form-row -->
            </div><!-- -/card-body-->
           

            <b-alert :show="dismissCountDown" dismissible variant="warning" @dismissed="dismissCountDown=0" @dismiss-count-down="countDownChanged">
                <p>Please wait, you will redirect to payment page after @{{ dismissCountDown }} seconds...</p>
                <b-progress variant="warning" :max="dismissSecs" :value="dismissCountDown" height="4px" ></b-progress>
            </b-alert>
            <b-modal ref="my-modal" hide-footer title="Error">
                <div class="d-block text-center">
                    <h3 variant="danger">Something wnents wrong!</h3>
                </div>
            </b-modal>
        </div><!-- /card-->
    </div>
</template>
@include('insurance.form')
@endsection