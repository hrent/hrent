<template id="form-invoice-template">
  <div class="modal" id="invoice_modal">
      <div class="modal-dialog modal-lg">
            <form action="#" @submit.prevent="onSubmit" method="POST" autocomplete="off">
               <div class="modal-content">
                    <div class="modal-header back-happyrent-light-green text-white">
                      <div class="modal-title">
                          @{{form.invoice_id ? 'Edit Invoice' : 'Add New Invoice'}}
                      </div>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                   <div class="modal-body">
                        <div class="row">
                            <div class="form-group col">
                                <label for="datefrom" class="control-label">Bill Date</label>
                                <div class="input-group">
                                <datetimepicker name="bill_date" v-model="form.bill_date" placeholder="Bill date" required></datetimepicker>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                    <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                </div>
                            </div>
                             <div class="form-group col">
                                <label for="due_date" class="control-label">Due Date</label>
                                <div class="input-group">
                                <datetimepicker name="due_date" v-model="form.due_date" placeholder="Due date" autocomplete="off" required></datetimepicker>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                    <i class="far fa-calendar-alt"></i>
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="datefrom" class="control-label">Property</label>
                                <div class="input-group">
                                    <select2 v-model="form.property_id" required>
                                        <option value="">All</option>
                                        <option v-for="property in property_options" :value="property.id">
                                          @{{property.name}}
                                    </select2>

                                </div>
                            </div>
                             <div class="form-group col">
                                <label for="due_date" class="control-label">Unit</label>
                                <div class="input-group">
                                     <select2 v-model="form.unit_id" required>
                                        <option value="">All</option>
                                        <option v-for="unit in unit_options" :value="unit.id">
                                          (@{{unit.property_name}}) @{{unit.block_number}} @{{unit.unit_number}} <span v-if="unit.address">,</span> @{{unit.address}}
                                    </select2>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Tax
                                </label>
                                <select2  name="tax"  class="form-control" v-model="form.tax">
                                    <option  v-for="(tax, index) in all_active_taxes" v-if="tax.deleted == 0" v-bind:value="tax.tax_id">@{{tax.name}} @{{tax.percentage}}%</option>
                                </select2>
                            </div>
                            <div class="form-group col">
                                <label class="control-label">
                                    Second Tax
                                </label>
                                <select2  name="tax"  class="form-control" v-model="form.tax_two">
                                    <option  v-for="(tax, index) in all_active_taxes" v-if="tax.deleted == 0" v-bind:value="tax.tax_id">@{{tax.name}} @{{tax.percentage}}%</option>
                                </select2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label class="control-label">
                                    Recuring
                                </label>
                                <input type="checkbox" id="recuring" v-model="form.recuring">
                                <i class="fa fa-question-circle" v-b-tooltip.hover title="Cron Job is required for this action!"></i>                            </div>

                        </div>
                        <div class="row" v-show="form.recuring">
                            <div class="col">
                                <label for="repeat_for">Repeat Every</label>
                                <input type="number" class="form-control" id="repeat_for" placeholder="" v-model="form.repeat_every">
                            </div>
                            <div class="col">
                                <label for="repeat_frequnecy">&nbsp;</label>
                                <select  name="frequency"  class="form-control" v-model="form.repeat_type">
                                    <option  value="day">Day(s)</option>
                                    <option  value="week">Week(s)</option>
                                    <option  value="month">Month(s)</option>
                                    <option  value="year">Year(s)</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" v-show="form.recuring">
                            <div class="col">
                                <label for="repeat_for">Cycle <i class="fa fa-question-circle" v-b-tooltip.hover title="Recurring will be stopped after the number of cycles. Keep it blank for infinity."></i></label>
                                <input type="number" class="form-control" name= "no_of_cycles" id="repeat_for" placeholder="" v-model="form.no_of_cycles">
                            </div>
                            <div class="col">
                                <label for="repeat_frequnecy"> &nbsp;</label>
                            </div>
                        </div>

                    </div>
                    <div class="form-group col-md-12 col-sm-12 col-xs-12" v-show="form.recuring">
                        <label class="control-label">
                            Note
                        </label>
                        <textarea name="note" class="form-control" v-model="form.note" rows="3"></textarea>
                    </div>
                    <div class="modal-footer">
                      <div class="btn-group">
                        <button type="submit" class="btn btn-success" v-if="!form.invoice_id">Create</button>
                        <button type="submit" class="btn btn-success" v-if="form.invoice_id">Update</button>
                        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                </form>
          </div>
      </div>
  </div>
</template>