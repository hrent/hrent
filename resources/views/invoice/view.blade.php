@extends('layouts.app')
@section('header')

@stop
@section('content')
<div>
   <div class="panel panel-default">
      <div class="panel-body screen-panel">
         <div id="indexInvoiceprocessController">
            <index-invoiceprocess>
            </index-invoiceprocess>
         </div>
      </div>
   </div>
</div>
<template id="index-invoiceprocess-template">
    <div>
       {{-- @can('read users') --}}
       <div class="card">
            <div class="card-header text-white">
                <div class="form-row">
                   <span class="mr-auto">

                    <a href="/invoices" style="color: white">
                       <i class="far fa-arrow-alt-circle-left" style="    font-size: x-large;
    padding-top: 45%;color: white"></i>

                    </a>
                    
                   </span>
                   <div v-if="invice_details.status !='pending' ">
                    <b-dropdown id="dropdown-1" text="Actions" class="m-md-2" variant="primary"  >
                        <b-dropdown-item v-if="grand_total && submitted_invoice" @click="submitInvoiceItem()"><i class="fas fa-receipt"></i> Send Invoice </b-dropdown-item>
                        <b-dropdown-item v-if="!grand_total" ><i class="fas fa-receipt"></i> No Invoice Item</b-dropdown-item>
                      <b-dropdown-item> <i class="far fa-envelope"></i> Email invoice to client</b-dropdown-item>
                      <b-dropdown-item><i class="fas fa-download"></i> Download PDF</b-dropdown-item>
                      <b-dropdown-item><i class="fas fa-search"></i> Invoice Preview</b-dropdown-item>
                      <b-dropdown-item><i class="fas fa-print"></i> Print invoice</b-dropdown-item>
                       
                      <b-dropdown-divider></b-dropdown-divider>
                      <b-dropdown-item><i class="fas fa-edit"></i> Edit invoice</b-dropdown-item>
                      <b-dropdown-item><i class="fas fa-check-square"></i> Mark as Not paid</b-dropdown-item>
                    </b-dropdown>
                    <b-button variant="primary" data-toggle="modal" data-target="#invoiceprocess_modal" @click="createSingleEntry"><i class="fa fa-plus"></i> Add Item</b-button>
                  </div>
                  
                </div>
            </div>
            <div class="card-body">
                <!-- Second panel-->
                <div class="panel panel-default  p15 no-border m0">
                   <span>
                      <b-badge variant="secondary" style="font-size: 100%;">@{{invice_details.status}}</b-badge>
                   </span>
                   <span class="ml15">Property:  <b-badge variant="info" style="font-size: 100%;">@{{invoice_property.name}}</b-badge></span>
                    <span class="ml15" v-if="invoice_unit.unit_number">Unit Number:  <b-badge variant="info" style="font-size: 100%;">@{{invoice_unit.unit_number}} </b-badge></span>
                 
                   
                </div>
                <!-- details invice-->
                <br>
                <!-- Invocide details-->
                <div class="row">
                   <div class="col-12">
                      <div class="card">
                         <div class="card-body p-0">
                            <div class="row p-3">
                               <div class="col-md-6">
                                  <img src="/img/icon.png" height="40" width="105">
                               </div>
                               <div class="col-md-6 text-right">
                                  <p class="font-weight-bold mb-1">
                                     <b-badge variant="success" style="font-size: 100%;">Invoice #@{{invice_details.invoice_id}}</b-badge>
                                  </p>
                                 
                               </div>


                                <div class="col-md-6" >
                                    <br>
                                  <p class="font-weight-bold mb-1">Bill To</p>
                                 
                                   
                                  <p class="mb-1">@{{invoice_property.name}}</p>
                                  <p class="mb-1">@{{invoice_unit.unit_number}}</p>
                                 
                                 
                               </div>
                               <div class="col-md-6 text-right">
                                 <p class="mb-1"><span class="font-weight-bold">Bill Date:</span> @{{invice_details.bill_date}}</p>
                                  <p class="mb-1"><span class="font-weight-bold">Due Date:</span> @{{invice_details.due_date}}</p>
                               </div>
                            </div>
                           
                            
                           <div class="table-responsive" v-if="grand_total">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                     <td class="text-center"><strong>#</strong></td>
                                    <td><strong>Item</strong></td>
                                    <td class="text-center"><strong>Price</strong></td>
                                    <td class="text-center"><strong>Tax</strong></td>
                                    <td class="text-right"><strong>Totals</strong></td>
                                    <td class="text-right" style="width:5%"></td>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                <tr v-for="(invoiceitem, index) in invoiceitems">
                                    
                                   <td class="text-center">@{{index + 1}}</td>
                                    <td v-if="invice_details.is_rental">@{{invoiceitem.title}}</td>
                                     <td class="text-center" v-else>-</td>
                                    <td class="text-center" v-if="invoiceitem.amount">{{$currency}}@{{invoiceitem.amount}}</td>
                                    <td class="text-center" v-else>{{$currency}}0.00</td>
                                    <td class="text-center" v-if="invoiceitem.tax_amount">{{$currency}}@{{invoiceitem.tax_amount}}</td>
                                     <td class="text-center" v-else>{{$currency}}0.00</td>
                                    <td class="text-right" v-if="invoiceitem.Total">{{$currency}}@{{ invoiceitem.Total}}</td>
                                    <td class="text-right" v-else>{{$currency}}0.00</td>
                                    <td >
                                       <!--  <a v-if="invice_details.status !='pending' || submitted_invoice" data-toggle="modal" data-target="#invoiceprocess_modal" @click="editSingleEntry(invoiceitem)"><i class="fas fa-pen"></i></a> -->
                                        <a  v-if="invice_details.status !='pending' && submitted_invoice"  @click="deactivateSingleEntry(invoiceitem)" data-action="delete"><i class="fa fa-times fa-fw"></i></a>
                                    </td>
                                </tr>
                              
                               
                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right" v-if="invice_details.subtotal_amount">{{$currency}}@{{invice_details.subtotal_amount}}</td>
                                    <td class="thick-line text-right" v-else>{{$currency}}0.00</td>
                                    <td class="thick-line"></td>

                                </tr>
                             
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Tax</strong></td>
                                    <td class="no-line text-right" v-if="invoice_taxable_amount">{{$currency}}@{{invoice_taxable_amount}}</td>
                                    <td  class="no-line text-right"v-else>{{$currency}}0.00</td>
                                </tr>
                                 <tr v-if="!invoice_taxable_amount">
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right" v-if="invice_details.payable_amount">{{$currency}}@{{invice_details.payable_amount}}</td>
                                    <td  class="no-line text-right"v-else>{{$currency}}0.00</td>
                                </tr>
                                 <tr v-else>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right" v-if="grand_total !=0">{{$currency}}@{{invice_details.payable_amount}}</td>
                                    <td  class="no-line text-right"v-else>{{$currency}}0.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive" v-if="!grand_total">
                      <center><h3>No Results Found</h3></center>
                    </div>
                          
                         </div>
                      </div>
                   </div>
                </div>
                <!-- ends invoice details-->

                  
            </div>
        </div>
        <br/>
        <!-- <div class="card">
            <div class="card-header text-white">
              <div class="form-row">
                 <span class="mr-auto">
                 Invoice payment list
                 </span>
              </div>
            </div>
            <div class="card-body">
              <div class="form-row" style="padding-top: 20px;">
                 <div class="table-responsive">
                 </div>
              </div>
            </div>
        </div> -->
      <form-invoiceprocess @updatetable="searchData" :data="formdata"></form-invoiceprocess>
     
   </div>
</template>
@include('invoice.add_invoice_item')
@endsection
<style type="text/css">
    .invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
.table-condensed td > a {
    cursor: pointer;
    min-width: 28px;
    background: #fff;
    color: #a1a3a5;
    border-radius: 50%;
    display: inline-block;
    position: relative;
    vertical-align: central;
    text-align: center;
    margin: 0 5px;
    padding: 4px 0px;
    border: 1px solid #e2e7f1;
}
.table-condensed td > a:hover {
    background: #2ac2d4;
    color: #fff;
    border: 1px solid #2ac2d4;
}
.table-condensed td > a.delete:hover {
    background: #d9534f;
    color: #fff;
    border: 1px solid #d9534f;
}
</style>