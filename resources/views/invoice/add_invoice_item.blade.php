<template id="form-invoiceprocess-template">
  <div class="modal" id="invoiceprocess_modal">
      <div class="modal-dialog modal-lg">
            <form action="#" @submit.prevent="onSubmit" method="POST" autocomplete="off">
               <div class="modal-content">
                    <div class="modal-header back-happyrent-light-green text-white">
                      <div class="modal-title">
                          @{{form.invoice_item_id ? 'Edit Invoice' : 'Add Invoice Item'}}
                      </div>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                   <div class="modal-body">
                        <div class="row">
                            <div class="form-group col">
                                 <label class="control-label">
                                    Item
                                </label>
                                <select  name="item_id"  @change="getItemType($event)"  class="form-control" v-model="form.item_id" required>
                                    
                                    <option  v-for="(item, index) in items"  v-bind:value="item.item_id">@{{item.title}}</option>
                                </select>
                            </div>
                            <div class="form-group col" v-if="is_item_rental">
                               <label class="control-label">
                                    Tenancy 
                                </label>
                                <i class="fa fa-question-circle" v-b-tooltip.hover title="Tenants must have added for thie unit!"></i> 
                               
                                <select  name="tenancy"  class="form-control"   @change="getTenancyDetails($event)" v-model="form.tenancy" required>
                                    
                                    <option  v-for="(tenancy, index) in tenancies"  v-bind:value="tenancy.rental + '_' + tenancy.arc_status + '_' + tenancy.tenancies_id ">@{{tenancy.name}} </option>
                                </select>

                            </div>
                             <div class="form-group col" v-else>
                               <label class="control-label">
                                    &nbsp;
                                </label>
                               

                            </div>
                             
                        </div>
                         <div class="row">
                            <div class="form-group col">
                                <label class="control-label">
                                    Tax
                                </label>
                                <select2  name="tax"  class="form-control" v-model="form.tax">
                                    <option  v-for="(tax, index) in all_active_taxes" v-if="tax.deleted == 0" v-bind:value="tax.tax_id">@{{tax.name}} @{{tax.percentage}}%</option>
                                </select2>
                            </div>
                            <div class="form-group col">
                                <label class="control-label">
                                    Second Tax
                                </label>
                                <select2  name="tax"  class="form-control" v-model="form.tax_two">
                                    <option  v-for="(tax, index) in all_active_taxes" v-if="tax.deleted == 0" v-bind:value="tax.tax_id">@{{tax.name}} @{{tax.percentage}}%</option>
                                </select2>
                            </div>
                        </div>
                        <div class="row">
                          
                            <div class="form-group col">
                                <label class="control-label">
                                    Is ARC Active? : <span v-if="arc_status == 1">
                                          Yes
                                       </span> 
                                        <span v-if="arc_status == 0">
                                          No
                                       </span> 
                                </label>
                                 
                            </div>
                             <div class="form-group col">
                                <label for="due_date" class="control-label">Rental :  <span v-if="rental">
                                          RM@{{rental}}
                                       </span><span v-if="rental == 0">
                                          RM0.00
                                       </span> </label>
                               
                            </div>
                        </div>

                        <div class="row">
                          
                            <div class="form-group col">
                                <label class="control-label">
                                    Description
                                </label>
                                <textarea name="note" class="form-control" v-model="form.description" rows="3" ></textarea>
                            </div>
                            <div class="form-group col">
                               &nbsp;
                            </div>
                        </div>
                    </div>
                  
                    <div class="modal-footer">
                      <div class="btn-group">
                        <button type="submit" class="btn btn-success" v-if="!form.invoice_id">Create</button>
                        <button type="submit" class="btn btn-success" v-if="form.invoice_id">Update</button>
                        <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                </form>
          </div>
      </div>
  </div>
</template>