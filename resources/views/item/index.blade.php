@extends('layouts.app')
@section('header')
    <a href="/items" class="btn btn-sm back-happyrent-light-green">
        <i class="fas fa-id-badge"></i>
        Items
    </a>
@stop
@section('content')
<div>
    <div class="panel panel-default">
        <div class="panel-body screen-panel">
            <div id="indexItemController">
                <index-item></index-item>
            </div>
        </div>
    </div>
</div>

<template id="index-item-template">
  <div>
    {{-- @can('read users') --}}
    <div class="card">
      <div class="card-header text-white">
          <div class="form-row">
          <span class="mr-auto">
              <i class="fas fa-id-badge"></i>
              Items
          </span>
          {{-- @can('create users') --}}
          <button type="button" class="btn btn-primary btn-sm ml-auto" data-toggle="modal" data-target="#item_modal" @click="createSingleEntry">
              <i class="fas fa-plus"></i>
          </button>
          {{-- @endcan --}}
          </div>
      </div>
      <div class="card-body">
        <form action="#" @submit.prevent="searchData" method="GET" autocomplete="off">
        <div class="form-row">
            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                <label for="title" class="control-label">Title</label>
                <input type="text" name="title" class="form-control" v-model="search.title" placeholder="title" autocomplete="off" @keyup="onFilterChanged">
            </div>
            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                <label for="email" class="control-label">Description</label>
                <input type="text" name="description" class="form-control" v-model="search.description" placeholder="description" autocomplete="off" @keyup="onFilterChanged">
            </div>
            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                <label for="unit_type" class="control-label">Unit Type</label>
                <input type="text" name="unit_type" class="form-control" v-model="search.unit_type" placeholder="unit type" autocomplete="off" @keyup="onFilterChanged">
            </div>

        </div>
        <div class="form-row">
            <div class="mr-auto">
                <div class="btn-group" role="group">
                    <button type="submit" class="btn btn-light btn-outline-dark">
                        <i class="fas fa-search"></i>
                        Search
                    </button>
                </div>
                <div class="form-row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="mr-auto">
                            <span class="font-weight-light" v-if="filterchanged">
                                <small>You have changed the filter, Search?</small>
                            </span>
                        </div>
                    </div>
                </div>
                <pulse-loader :loading="searching" :height="50" :width="100" style="padding-top:5px;"></pulse-loader>
            </div>
            <div class="ml-auto">
                <div>
                    <label for="display_num">Display</label>
                    <select v-model="selected_page" name="pageNum" @change="fetchTable">
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                        <option value="All">All</option>
                    </select>
                    <label for="display_num2" style="padding-right: 20px">per Page</label>
                </div>
                <div>
                    <label class="" style="padding-right:18px;" for="totalnum">Showing @{{list.length}} of @{{pagination.total}} entries</label>
                </div>
            </div>
        </div>
        </form>

        <div class="form-row" style="padding-top: 20px;">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="table-secondary">
                        <th class="text-center">
                            #
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('title')">Title</a>
                            <span v-if="sortkey == 'title' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'title' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('description')">Description</a>
                            <span v-if="sortkey == 'description' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'description' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('unit_type')">Unity Type</a>
                            <span v-if="sortkey == 'unit_type' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'unit_type' && reverse" class="fa fa-caret-up"></span>
                        </th>
                       
                        <th class="text-center">
                            <a href="#" @click="sortBy('status')">Status</a>
                            <span v-if="sortkey == 'status' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'status' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th></th>
                    </tr>

                    <tr v-for="(data, index) in list" class="row_edit">
                        <td class="text-center">
                            @{{ index + pagination.from }}
                        </td>
                        <td class="text-left">
                            @{{ data.title }}
                        </td>
                        <td class="text-center">
                            @{{ data.description }}
                        </td>
                        <td class="text-center">
                            @{{ data.unit_type }}
                        </td>
                       
                        <td class="text-center">
                            <label class="switch" v-if="data.status == 1">
                              <input @change="changeUserStatus(data)" type="checkbox" checked >
                              <span class="slider round"></span>
                            </label>
                            <label class="switch"  v-else>
                              <input type="checkbox"  @change="changeUserStatus(data)">
                              <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="text-center">
                           
                            <div class="btn-group">
                            <button type="button" class="btn btn-light btn-outline-secondary btn-sm" data-toggle="modal" data-target="#item_modal" @click="editSingleEntry(data)">
                                <i class="fas fa-edit"></i>
                            </button>
                          
                             <button type="button" v-if="data.deleted == 1"  class="btn btn-light btn-outline-secondary btn-sm"  @click="restoreSingleEntry(data)">
                             <i class="fa fa-history" aria-hidden="true"></i>
                            </button>
                           
                            <button type="button" v-if="data.deleted == 0"   class="btn btn-danger btn-sm" @click="deactivateSingleEntry(data)">
                                <i class="fas fa-trash"></i>
                            </button>
                            </div>
                        </td>
                    </tr>
                    <tr v-if="! pagination.total">
                        <td colspan="18" class="text-center"> No Results Found </td>
                    </tr>
                </table>
            </div>
            <div class="pull-left">
                <pagination :pagination="pagination" :callback="fetchTable" :offset="4"></pagination>
            </div>
        </div>
      </div>
    </div>
{{--
    @else
        @include('error.unauthorised')
    @endcan --}}
    <form-item @updatetable="searchData" :data="formdata"></form-item>

  </div>
</template>

@include('item.form')

@endsection
<style>
.switch {
  position: relative;
  display: inline-block;
   width: 50px;
  height: 24px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #FF0000;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 15px;
  width: 15px;
  left: 4px;
  bottom: 5px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #008000;
}



input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>