<template id="form-item-template">
  <div class="modal" id="item_modal">
      <div class="modal-dialog modal-lg">
          <form action="#" @submit.prevent="onSubmit" method="POST" autocomplete="off">
          <div class="modal-content">
              <div class="modal-header back-happyrent-light-green text-white">
                  <div class="modal-title">
                      @{{form.item_id ? 'Edit Item' : 'Add new Item'}}
                  </div>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label">
                        Title
                    </label>
                   <input type="text" name="title" class="form-control" v-model="form.title" required>
                </div>
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label">
                        Description
                    </label>
                    <textarea name="description" class="form-control" v-model="form.description" rows="5"></textarea>
                </div>
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label">
                        Unit Type
                    </label>
                    <select  name="unit_type"class="form-control" v-model="form.unit_type" required>
                    <option   value="Monthly">Monthly</option>
                    <option   value="Yearly">Yearly </option>
                </select>
                </div>
                
                 <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label">
                        Item Code
                    </label>
                   <input type="text" name="item_code" class="form-control" v-model="form.item_code" required>
                </div>
              </div>
              <div class="modal-footer">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-success" v-if="!form.item_id">Create</button>
                    <button type="submit" class="btn btn-success" v-if="form.item_id">Update</button>
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                  </div>
              </div>
              </form>
          </div>
      </div>
  </div>
</template>