@extends('layouts.app')
@section('header')
    <a href="/transactions" class="btn btn-sm back-happyrent-light-green">
        <i class="fas fa-list"></i>
        Transactions
    </a>
@stop
@section('content')
<div>
    <div class="panel panel-default">
        <div class="panel-body screen-panel">
            <div id="indexTransactionsController">
                <index-transactions></index-transactions>
            </div>
        </div>
    </div>
</div>

<template id="index-transactions-template">
  <div>
    <div class="card">
      <div class="card-header text-white">
          <div class="form-row">
          <span class="mr-auto">
              <i class="fas fa-list"></i>
              Transactions
          </span>
         
          </div>
      </div>
      <div class="card-body">
        <div class="form-row" style="padding-top: 20px;">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr class="table-secondary">
                        <th class="text-center">
                            #
                        </th>
                        <!-- <th class="text-center">
                            <a href="#" @click="sortBy('block_number')">Block Number</a>
                            <span v-if="sortkey == 'block_number' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'block_number' && reverse" class="fa fa-caret-up"></span>
                        </th> -->
                        <th class="text-center">
                            <a href="#" @click="sortBy('unit_number')">Unit Number</a>
                            <span v-if="sortkey == 'unit_number' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'unit_number' && reverse" class="fa fa-caret-up"></span>
                        </th>
                      
                        <th class="text-center">
                            <a href="#" @click="sortBy('property_id')">Property</a>
                            <span v-if="sortkey == 'property_id' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'property_id' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('property_id')">Plan</a>
                            <span v-if="sortkey == 'property_id' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'property_id' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('property_id')">Mode</a>
                            <span v-if="sortkey == 'property_id' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'property_id' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('insurance_status')">Status</a>
                            <span v-if="sortkey == 'insurance_status' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'insurance_status' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        <th class="text-center">
                            <a href="#" @click="sortBy('block_number')">Date</a>
                            <span v-if="sortkey == 'block_number' && !reverse" class="fa fa-caret-down"></span>
                            <span v-if="sortkey == 'block_number' && reverse" class="fa fa-caret-up"></span>
                        </th>
                        
                    </tr>
                   
                   
                    <tr v-for="(data, index) in list" class="row_edit">
                        <td class="text-center">
                            @{{ index + pagination.from }}
                        </td>
                       
                        <td class="text-center">
                            @{{ data.unit_number }}
                        </td>
                      
                        <td class="text-center">
                            @{{ data.property_name }}
                        </td>
                        <td class="text-center">
                            @{{ data.property_name }}
                        </td>
                        <td class="text-center">
                            fpx
                        </td>
                        <td class="text-center" v-if="data.insurance_status == 1" >
                            <span class="badge badge-success">
                                Success
                            </span>
                            
                        </td>
                        <td class="text-center" v-else>
                            <span class="badge badge-danger">
                                Pending
                            </span>
                        </td>
                        <td class="text-center">
                            @{{ data.created_at }}
                        </td>
                      
                    </tr>
                   <tr v-if="! pagination.total">
                        <td colspan="18" class="text-center"> No Results Found </td>
                    </tr>
                    
                </table>
               
            </div>
            <div class="pull-left">
                <pagination :pagination="pagination" :callback="fetchTable" :offset="4"></pagination>
            </div>
            <div class="pull-right" v-if="showplanselected.plantotal !=0" style="margin-left: 63%;">
                <b>Total Amount: @{{showplanselected.plantotal}} RM </b> <br><br>
                <a href="/checkout" class="btn btn-primary btn-md ml-auto" style="width: 150px;">
                    Continue
                </a>
                
                
            </div>
            
        </div>
      </div>
      </div>
    </div>
   
  </div>
</template>
@endsection
