require('./bootstrap');
require('./form');
require('./error');

window.Vue = require('vue');

// npm elements
window.select2 = require('select2');
window.moment = require('moment');
window.events = new Vue();
window.flash = function (message, level = 'info') {
  window.events.$emit('flash', {message, level});
};

require('bootstrap-datepicker');
window.accounting = require('accounting-js');
$.fn.datetimepicker = require('eonasdan-bootstrap-datetimepicker');



// vue reusable components
// modal for vue
import BootstrapModal from 'vue2-bootstrap-modal';
Vue.component('bootstrap-modal', BootstrapModal);

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue)

// date and datetimepicker
import Datepicker from 'vuejs-datepicker';
Vue.component('datepicker', Datepicker);

import Datetimepicker from './components/Datetimepicker.vue';
Vue.component('datetimepicker', Datetimepicker);

// flash vue
import Flash from './components/Flash.vue';
Vue.component('flash', Flash);

// This imports all the layout components such as <b-container>, <b-row>, <b-col>:
import { Layout } from 'bootstrap-vue/es/components'
Vue.use(Layout)

// This imports <b-modal> as well as the v-b-modal directive as a plugin:
import { Modal } from 'bootstrap-vue/es/components'
Vue.use(Modal)

// vue multiselect
import Multiselect from 'vue-multiselect';
Vue.component('multiselect', Multiselect);

// This imports <b-card> along with all the <b-card-*> sub-components as a plugin:
import { Card } from 'bootstrap-vue/es/components'
Vue.use(Card)

// This imports directive v-b-scrollspy as a plugin:
/*
import { Scrollspy } from 'bootstrap-vue/es/directives'
Vue.use(Scrollspy) */
import Pagination from './components/Pagination.vue';
Vue.component('pagination', Pagination);

// vue js loading
import PulseLoader from 'vue-spinner/src/PulseLoader.vue';
Vue.component('pulse-loader', PulseLoader);

// select2 vue
import Select2 from './components/Select2.vue';
Vue.component('select2', Select2);

import 'vue2-dropzone/dist/vue2Dropzone.min.css';
import vue2Dropzone from 'vue2-dropzone';
Vue.component('vueDropzone', vue2Dropzone);


// vue modal
import VModal from 'vue-js-modal'
Vue.component('v-modal', VModal);

// vue select
import vSelect from 'vue-select';
Vue.component('v-select', vSelect);
// This imports directive v-b-scrollspy as a plugin:
/*
import { Scrollspy } from 'bootstrap-vue/es/directives'
Vue.use(Scrollspy) */

/*
import FlashMessage from 'smartweb/vue-flash-message';
Vue.use(FlashMessage); */


// object oriented form and errors prompt
require('./form');
require('./error');

// vue reusable filters
require('./filters/boolean-filter');
require('./filters/currency-filter');
require('./filters/moment-humanize');
require('./filters/number-monthname');

// vue directives
// require('./directives/select-select2');

// vue controllers
require('./controllers/indexTenancyController');
require('./controllers/indexPermissionController');
require('./controllers/indexProfileController');
require('./controllers/indexPropertyController');
require('./controllers/indexTenantController');
require('./controllers/indexBeneficiaryController');
require('./controllers/indexInsuranceController');
require('./controllers/indexRoleController');
require('./controllers/indexUnitController');
require('./controllers/indexUserController');
require('./controllers/accountUserController');
require('./controllers/indexCheckoutController');
require('./controllers/indexTenantUtilityrecordController');
require('./controllers/indexUtilityrecordController');
require('./controllers/indexTransactionsController');
require('./controllers/indexItemController');
require('./controllers/indexTaxController');
require('./controllers/indexInvoiceController');
require('./controllers/indexAgencyController');
require('./controllers/indexInvoiceprocessController');





$(".sidebar-dropdown > a").click(function () {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$("#close-sidebar").click(function () {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function () {
  $(".page-wrapper").addClass("toggled");
});

