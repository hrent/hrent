if (document.querySelector('#indexUserController')) {
  Vue.component('index-user', {
    template: '#index-user-template',
    data() {
      return {
            list: [],
            userroles:[],
            search: {
              name: '',
              email: '',
              phone_number: ''
            },
            searching: false,
            sortkey: '',
            reverse: false,
            selected_page: '100',
            pagination: {
              total: 0,
              from: 1,
              per_page: 1,
              current_page: 1,
              last_page: 0,
              to: 5
            },
            formdata: {},
            exiformdata: {},
            filterchanged: false,
            errors: [],

            showdata: {},
            idtype_id:false,
            clearform: {}

        }
    },
    mounted() {
      this.fetchTable();
    },
    methods: {
      fetchTable() {
        this.searching = true;
       this.errors= [];
        let data = {
          paginate: this.pagination.per_page,
          page: this.pagination.current_page,
          sortkey: this.sortkey,
          reverse: this.reverse,
          per_page: this.selected_page,
          name: this.search.name,
          email: this.search.email,
          phone_number: this.search.phone_number
        };
        axios.get(
          // subject to change (search list and pagination)
          '/api/users?page=' + data.page +
          '&perpage=' + data.per_page +
          '&sortkey=' + data.sortkey +
          '&reverse=' + data.reverse +
          '&name=' + data.name +
          '&email=' + data.email +
          '&phone_number=' + data.phone_number
        ).then((response) => {
          const result = response.data;
          if (result) {
            this.list = result.data;
            this.pagination = {
              total: result.total,
              from: result.from,
              to: result.to,
              per_page: result.per_page,
              current_page: result.current_page,
              last_page: result.last_page,
            }
          }
        });
        this.searching = false;

      },
      searchData() {
        this.pagination.current_page = 1;
        this.fetchTable();
        this.filterchanged = false;
      },
      sortBy(sortkey) {
        this.pagination.current_page = 1;
        this.reverse = (this.sortkey == sortkey) ? !this.reverse : false;
        this.sortkey = sortkey;
        this.fetchTable();
      },
      createSingleEntry() {
        this.clearform = {}
        this.formdata = ''
      },
      editSingleEntry(data) {
        console.log(data);
        this.idtype_id = data.id_type;
        this.formdata = '';
        this.formdata = data
      },
      showUserData(data) {

        this.showdata = '';
        this.showdata = data
      },
      deactivateSingleEntry(data) {
        var approval = confirm('Are you sure to delete ' + data.name + '?')
        if (approval) {
          axios.delete('/api/user/' + data.id + '/deactivate').then((response) => {
            this.searchData();

          });
        } else {
          return false;
        }
      },
      restoreSingleEntry(data) {
        var approval = confirm('Are you sure to restore ' + data.name + '?')
        if (approval) {
          axios.post('/api/user/' + data.id + '/restore').then((response) => {
            this.searchData();

          });
        } else {
          return false;
        }
      },
       changeUserStatus(data) {

          axios.post('/api/user/status',{
            user_id : data.id,
            status: data.status
          }).then((response) => {
            console.log(response.data);
            this.searchData();

          });

      },

      onFilterChanged() {
        this.filterchanged = true;
      },
      ////////////////////
       fetchUserRoles(data) {

         axios.get('/api/user/'+data.id+'/roles' ).then((response) => {

            this.assign_roles = response.data;
            return  this.assign_roles;



          });
        },
      //////////////////////
    },
    watch: {
      'selected_page'(val) {
        this.selected_page = val;
        this.pagination.current_page = 1;
        this.fetchTable();
      }
    }
  });

  Vue.component('form-user', {
    template: '#form-user-template',
    props: ['data', 'clearform'],
    errors: [],
    data() {

      return {
        form: {
          id: '',
          name: '',
          role_id: '',
          email: '',
          phone_number: '',
          password: '',
          password_confirmation: '',
          tenancy_id: '',
          idtype_id: '',
          id_value: '',
          slug:'',


        },
        exeform: {
          email: '',
          phone_number: '',
          errors: [],
        },
        exeroleform: {
          role_id: '',
          user_id: '',

        },
         agencyform: {

          address: '',
           latitude: '',
          longitude: '',
           country: '',
          state: '',
          city: '',
        },
        formErrors: {},
        errors:[],
        userroles:this.userroles,
        idtype_options: [],
        existingSubmit:false,
        tabIndex:1,
        existinguser:{},
        filteredroles:[],


        countries:[],
        states:[],
        cities:[],
        last_inserted_id:'',
        last_inserted_role:'',
      }
    },
    mounted() {
      this.fetchIdtypeSearchOptions();
      this.fetchRolesOptions();
      this.getCountries();
    },
    methods: {

        onSubmit(e) {
          axios.post('/api/user/store-update', this.form).then((response) => {
              if(response.data.error == 0){
                this.last_inserted_id = response.data.user_id;
                this.last_inserted_role = response.data.userrole;


                $('.modal').modal('hide');
                for (var key in this.form) {
                  this.form[key] = null;
                }
                this.$emit('updatetable')
              }else{
                alert('Something went wrong!');
              }

              });
        },
        onExistingSubmit() {
             console.log(this.exeform);
            axios.post('/api/user/existing-store-update', this.exeform).then((response) => {
              this.existinguser = response.data;
              this.filteredroles = response.data.newroles;
              console.log(this.existinguser);
              if (response.data.error == 0) {
                this.existingSubmit = true;
                for (var key in this.exeform) {

                  this.exeform[key] = null;
                }
                this.$emit('updatetable')
              }else{
                alert(response.data.error);
              }

            });
        },
        existingUserNewRoleSubmit() {
            this.exeroleform.user_id=this.existinguser.id;
             console.log(this.exeroleform);
            axios.post('/api/user/add-more-role', this.exeroleform).then((response) => {
              $('.modal').modal('hide');
              console.log(this.exeroleform);

              this.existingSubmit = false;
              for (var key in this.exeroleform) {

                this.exeroleform[key] = null;
              }
              this.$emit('updatetable')
            });
        },

        fetchIdtypeSearchOptions() {
            axios.get('/api/idtypes/all').then((response) => {
              this.idtype_options = response.data
            });
        },
        fetchRolesOptions() {
         axios.get('/api/getroles' ).then((response) => {
            this.userroles = response.data;

          });
        },
        imagePreview(event) {
            let input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                let vm = this;
                reader.onload = e => {
                    this.previewImageUrl = e.target.result;
                    vm.image = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
        storeAgencyDetails(){
            axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
            axios.post('/api/user/store-agency-details', {
                image: this.image,
                user_id:this.last_inserted_id,
                role_id:this.last_inserted_role,
                agency_details: this.agencyform
            }).then(response => {
                $('.modal').modal('hide');

            }).catch(e => {
                console.log(e);
            })
        },
        //get countries
        getCountries() {
            axios.get('/api/countries' ).then((response) => {
                this.countries = response.data;

            });
        },
        //call function from county select
        getState(event) {
            this.states = [];
            this.cities = [];
            axios.post('/api/states',{
              country_id :event.target.value,
            } ).then((response) => {
                this.states = response.data;

            });
        },
        //call function from sate drop down
        getCity(event) {
            this.cities = [];
            axios.post('/api/cities',{
            state_id :event.target.value,
            } ).then((response) => {
                this.cities = response.data;

            });
        },

      ///////////
    },
    watch: {
      'data'(val) {
        for (var key in this.form) {
          this.form[key] = this.data[key];
        }
        for (var key in this.exeform) {
          this.exeform[key] = this.data[key];
        }


      }

    }
  });
    //////////////
    Vue.component('show-user', {
    template: '#show-user-template',
    props: ['data'],
     errors: [],
    data() {

      return {
        form: {
          id: '',
          name: '',
          userrole: '',
          email: '',
          phone_number: '',
          password: '',
          password_confirmation: '',
            tenancy_id: '',
          idtype_id: '',
          id_value: '',

        },

      }
    },
     mounted() {
       this.testdet();
    //   this.fetchRolesOptions();
    //   this.getCountries();
    },
    methods: {
        showUserData() {
        this.form = '';
      },
        getRoleChange: function(event){
            console.log(event.target.value);
            // if (event.target.value == 2) {
            //   this.agancy_next = true;
            // }else{
            //   this.agancy_next = false;
            // }
        },
        testdet: function(){
           // alert('hecccccccMunnacccccre');
        },


      ///////////
    },
    watch: {
      'data'(val) {
        for (var key in this.form) {
          this.form[key] = this.data[key];
        }
        for (var key in this.exeform) {
          this.exeform[key] = this.data[key];
        }
      },
      'clearform'(val) {
        if(val) {
          this.formErrors = val
        }
      }

    }
  });
    //////////////
  new Vue({
    el: '#indexUserController',
  });
}