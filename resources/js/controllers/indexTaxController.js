if (document.querySelector('#indexTaxController')) {
  Vue.component('index-tax', {
    template: '#index-tax-template',
    data() {
      return {
            list: [],
            search: {
              name: '',
              percentage: ''
             
            },
            searching: false,
            sortkey: '',
            reverse: false,
            selected_page: '100',
            pagination: {
              total: 0,
              from: 1,
              per_page: 1,
              current_page: 1,
              last_page: 0,
              to: 5
            },
            formdata: {},
            filterchanged: false,
            showdata: {},

        }
    },
    mounted() {
      this.fetchTable();
    },
    methods: {
      fetchTable() {
        this.searching = true;
      
        let data = {
          paginate: this.pagination.per_page,
          page: this.pagination.current_page,
          sortkey: this.sortkey,
          reverse: this.reverse,
          per_page: this.selected_page,
          name: this.search.name,
          percentage: this.search.percentage,
          
        };
        axios.get(
          // subject to change (search list and pagination)
          '/api/taxes?page=' + data.page +
          '&perpage=' + data.per_page +
          '&sortkey=' + data.sortkey +
          '&reverse=' + data.reverse +
          '&name=' + data.name +
          '&percentage=' + data.percentage
        ).then((response) => {
          const result = response.data;
          if (result) {
            this.list = result.data;
            this.pagination = {
              total: result.total,
              from: result.from,
              to: result.to,
              per_page: result.per_page,
              current_page: result.current_page,
              last_page: result.last_page,
            }
          }
        });
        this.searching = false;
       
      },
      searchData() {
        this.pagination.current_page = 1;
        this.fetchTable();
        this.filterchanged = false;
      },
      sortBy(sortkey) {
        this.pagination.current_page = 1;
        this.reverse = (this.sortkey == sortkey) ? !this.reverse : false;
        this.sortkey = sortkey;
        this.fetchTable();
      },
      createSingleEntry() {
        this.formdata = '';
      },
      editSingleEntry(data) {

        this.formdata = '';
        this.formdata = data
      },
      showUserData(data) {
       
        this.showdata = '';
        this.showdata = data
      },
      deactivateSingleEntry(data) {
        var approval = confirm('Are you sure to delete ' + data.name + '?')
        if (approval) {
          axios.delete('/api/tax/' + data.tax_id + '/deactivate').then((response) => {
            this.searchData();
             
          });
        } else {
          return false;
        }
      },
      restoreSingleEntry(data) {
        var approval = confirm('Are you sure to restore ' + data.name + '?')
        if (approval) {
          axios.get('/api/tax/' + data.tax_id + '/restore').then((response) => {
            this.searchData();
            //window.location.reload()
          });
        } else {
          return false;
        }
      },
       changeTaxStatus(data) {
   
          axios.post('/api/tax/status',{
            tax_id : data.tax_id,
            status: data.status
          }).then((response) => {
             this.searchData();
           // window.location.reload()
          });
       
      },

      onFilterChanged() {
        this.filterchanged = true;
      }

    },
    watch: {
      'selected_page'(val) {
        this.selected_page = val;
        this.pagination.current_page = 1;
        this.fetchTable();
      }
    }
  });

  Vue.component('form-tax', {
    template: '#form-tax-template',
    props: ['data'],
    
    data() {
      
      return {
        form: {
            tax_id: '',
            name: '',
            percentage: ''
           
            
        },
        tabIndex:1,
        last_inserted_id:'',
      
      }
    },
    mounted() {
     
    },
    methods: {
        getRoleChange: function(event){
            console.log(event.target.value);
            if (event.target.value == 2) {
              this.agancy_next = true;
            }else{
              this.agancy_next = false;
            }
        },

        onSubmit(e) {
            console.log(this.form);
            axios.post('/api/tax/store-update', this.form).then((response) => {
                  if(response.data.error == 0){
                    this.last_inserted_id = response.data.tax_id;
                    
                     $('.modal').modal('hide');
                    for (var key in this.form) {
                      this.form[key] = null;
                    }
                    this.$emit('updatetable')
                  }else{
                    alert(response.data.error);
                  }
                 
                });
        },
        },
    watch: {
      'data'(val) {
            for (var key in this.form) {
              this.form[key] = this.data[key];
            }
        }

    }
  });
   
    //////////////
  new Vue({
    el: '#indexTaxController',
  });
}