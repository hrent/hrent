if (document.querySelector('#indexAgencyController')) {
  Vue.component('index-agency', {
    template: '#index-agency-template',
    data() {
      return {
            list: [],
            search: {
                id: '',
                name: '',
                address1: '',
                created_at: '',
                is_active:'',


            },
            searching: false,
            sortkey: '',
            reverse: false,
            selected_page: '100',
            pagination: {
              total: 0,
              from: 1,
              per_page: 1,
              current_page: 1,
              last_page: 0,
              to: 5
            },
            formdata: {},
            filterchanged: false,
            showdata: {},
            base_url:'',

        }
    },
    mounted() {
      this.fetchTable();

    },
    methods: {
      fetchTable() {

        this.searching = true;

        let data = {
            paginate: this.pagination.per_page,
            page: this.pagination.current_page,
            sortkey: this.sortkey,
            reverse: this.reverse,
            per_page: this.selected_page,
            id: this.search.id,
            name: this.search.name,
            address1: this.search.address1,
            created_at: this.search.created_at,
            is_active: this.search.is_active,


        };
        axios.get(
          // subject to change (search list and pagination)
          '/api/agencies?page=' + data.page +
          '&perpage=' + data.per_page +
          '&sortkey=' + data.sortkey +
          '&reverse=' + data.reverse +
          '&id=' + data.id +
           '&name=' + data.name +
           '&address1=' + data.address1 +

            '&created_at=' + data.created_at +
            '&is_active=' + data.is_active
        ).then((response) => {
          const result = response.data.agencies;
          const base_url =  response.data.base_url;
          if (result) {
            this.list = result.data;
            this.base_url = base_url;
            this.pagination = {
              total: result.total,
              from: result.from,
              to: result.to,
              per_page: result.per_page,
              current_page: result.current_page,
              last_page: result.last_page,
            }
          }
        });
        this.searching = false;

      },
      searchData() {
        this.pagination.current_page = 1;
        this.fetchTable();
        this.filterchanged = false;
      },
      sortBy(sortkey) {
        this.pagination.current_page = 1;
        this.reverse = (this.sortkey == sortkey) ? !this.reverse : false;
        this.sortkey = sortkey;
        this.fetchTable();
      },
      createSingleEntry() {
        this.formdata = '';
      },
      createAgencyAdmin(data) {
        this.formdata = '';
        this.formdata = data
      },
      editSingleEntry(data) {

        this.formdata = '';
        this.formdata = data
      },
      showUserData(data) {

        this.showdata = '';
        this.showdata = data
      },
     deactivateSingleAgency(data) {
        var approval = confirm('Are you sure to delete ' + data.name + '?')
        if (approval) {
          axios.delete('/api/agencies/' + data.id + '/deactivate').then((response) => {
            this.searchData();

          });
        } else {
          return false;
        }
      },
      restoreSingleAgency(data) {
        var approval = confirm('Are you sure to restore ' + data.name + '?')
        if (approval) {
          axios.post('/api/agencies/' + data.id + '/restore').then((response) => {
            this.searchData();

          });
        } else {
          return false;
        }
      },
       changeAgencyStatus(data) {

          axios.post('/api/agencies/status',{
            agency_id : data.id,
            status: data.is_active
          }).then((response) => {
            console.log(response.data);
            this.searchData();

          });

      },

      onFilterChanged() {
        this.filterchanged = true;
      },


    },
    watch: {
      'selected_page'(val) {
        this.selected_page = val;
        this.pagination.current_page = 1;
        this.fetchTable();
      }
    }
  });

  Vue.component('form-agency', {
    template: '#form-agency-template',
    props: ['data'],

    data() {

      return {

        form: {
          id:'',
          name:'',
          address1: '',
          address2: '',
          location:'',
          latitude: '',
          longitude: '',
          country_id: '',
          state_id: '',
          city_id: '',
          logo:'',
          prefix: ''
        },

        countries:[],
        states:[],
        cities:[],
        tabIndex:1,
        previewImageUrl:'',
        base_url:'',


      }
    },
    mounted() {
     this.getCountries();
     this.getBaseUrl();
    },
    methods: {

           imagePreview(event) {
            let input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                let vm = this;
                reader.onload = e => {
                    this.previewImageUrl = e.target.result;
                    console.log(this.previewImageUrl);
                    vm.image = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);
            }
        },
            onMemberSubmit(){
            axios.defaults.headers.post['Content-Type'] = 'multipart/form-data';
            axios.post('/api/agencies/store-update', {
                image: this.image,
                agency_details: this.form
            }).then(response => {
                $('.modal').modal('hide');
               this.$emit('updatetable')
            }).catch(e => {
                console.log(e);
            })
        },


        //get countries
        getBaseUrl() {
            axios.get('/api/get-base-url' ).then((response) => {
                this.base_url = response.data;

            });
        },
        //get countries
        getCountries() {
            axios.get('/api/countries' ).then((response) => {
                this.countries = response.data;

            });
        },
        //call function from county select
        getState(event) {
            this.states = [];
            this.cities = [];
            axios.post('/api/states',{
              country_id :event.target.value,
            } ).then((response) => {
                this.states = response.data;

            });
        },
        //call function from sate drop down
        getCity(event) {
            this.cities = [];
            axios.post('/api/cities',{
            state_id :event.target.value,
            } ).then((response) => {
                this.cities = response.data;

            });
        },
        },
    watch: {
      'data'(val) {
            for (var key in this.form) {
              this.form[key] = this.data[key];

            }
             //set states
            axios.post('/api/states',{
              country_id :this.form.country_id,
            } ).then((response) => {
                this.states = response.data;
                  
            });
            //set cities
             axios.post('/api/cities',{
            state_id :this.form.state_id,
            } ).then((response) => {
                this.cities = response.data;
              
            });

        }

    }
  });
   Vue.component('agency-member', {
    template: '#agency-member-template',
    props: ['data'],

    data() {

      return {

        form: {
          name:'',
          member_id:'',
          id:''


        },
        members:[],

      }
    },
    mounted() {
     this.getAgencyMembers();

    },
    methods: {
        getRoleChange: function(event){
            console.log(event.target.value);
            if (event.target.value == 2) {
              this.agancy_next = true;
            }else{
              this.agancy_next = false;
            }
        },
        //get countries
        getAgencyMembers() {
            axios.get('/api/agencies/members' ).then((response) => {
                this.members = response.data;

            });
        },
        onMemberSubmit() {

                console.log(this.form);
            axios.post('/api/agencies/add-member-to-agency', this.form).then((response) => {
                if(response.data.status == 'success'){

                    $('.modal').modal('hide');
                    alert(response.data.message);
                    for (var key in this.form) {
                      this.form[key] = null;
                    }
                        this.$emit('updatetable')
                }else{
                    alert(response.data.message);
                }
             this.$emit('updatetable')
            });
        },

        },
       watch: {
      'data'(val) {
            for (var key in this.form) {
              this.form[key] = this.data[key];
            }
        }

    }
  });
    //////////////
  new Vue({
    el: '#indexAgencyController',
  });
}