if (document.querySelector('#indexItemController')) {
  Vue.component('index-item', {
    template: '#index-item-template',
    data() {
      return {
            list: [],
            search: {
              title: '',
              description: '',
              unit_type: ''
            },
            searching: false,
            sortkey: '',
            reverse: false,
            selected_page: '100',
            pagination: {
              total: 0,
              from: 1,
              per_page: 1,
              current_page: 1,
              last_page: 0,
              to: 5
            },
            formdata: {},
            filterchanged: false,
            showdata: {},

        }
    },
    mounted() {
      this.fetchTable();
    },
    methods: {
      fetchTable() {
        this.searching = true;
      
        let data = {
          paginate: this.pagination.per_page,
          page: this.pagination.current_page,
          sortkey: this.sortkey,
          reverse: this.reverse,
          per_page: this.selected_page,
          title: this.search.title,
          description: this.search.description,
          unit_type: this.search.unit_type
        };
        axios.get(
          // subject to change (search list and pagination)
          '/api/items?page=' + data.page +
          '&perpage=' + data.per_page +
          '&sortkey=' + data.sortkey +
          '&reverse=' + data.reverse +
          '&title=' + data.title +
          '&description=' + data.description +
          '&unit_type=' + data.unit_type
        ).then((response) => {
          const result = response.data;
          if (result) {
            this.list = result.data;
            this.pagination = {
              total: result.total,
              from: result.from,
              to: result.to,
              per_page: result.per_page,
              current_page: result.current_page,
              last_page: result.last_page,
            }
          }
        });
        this.searching = false;
       
      },
      searchData() {
        this.pagination.current_page = 1;
        this.fetchTable();
        this.filterchanged = false;
      },
      sortBy(sortkey) {
        this.pagination.current_page = 1;
        this.reverse = (this.sortkey == sortkey) ? !this.reverse : false;
        this.sortkey = sortkey;
        this.fetchTable();
      },
      createSingleEntry() {
        this.formdata = '';
      },
      editSingleEntry(data) {

        this.formdata = '';
        this.formdata = data
      },
      showUserData(data) {
       
        this.showdata = '';
        this.showdata = data
      },
      deactivateSingleEntry(data) {
        var approval = confirm('Are you sure to delete ' + data.title + '?')
        if (approval) {
          axios.delete('/api/item/' + data.item_id + '/deactivate').then((response) => {
            this.searchData();
             
          });
        } else {
          return false;
        }
      },
      restoreSingleEntry(data) {
        var approval = confirm('Are you sure to restore ' + data.title + '?')
        if (approval) {
          axios.get('/api/item/' + data.item_id + '/restore').then((response) => {
            this.searchData();
            //window.location.reload()
          });
        } else {
          return false;
        }
      },
       changeItemStatus(data) {
   
          axios.post('/api/item/status',{
            item_id : data.item_id,
            status: data.status
          }).then((response) => {
             this.searchData();
           // window.location.reload()
          });
       
      },

      onFilterChanged() {
        this.filterchanged = true;
      }

    },
    watch: {
      'selected_page'(val) {
        this.selected_page = val;
        this.pagination.current_page = 1;
        this.fetchTable();
      }
    }
  });

  Vue.component('form-item', {
    template: '#form-item-template',
    props: ['data'],
    
    data() {
      
      return {
        form: {
            item_id: '',
            title: '',
            description: '',
            unit_type: '',
            item_code: ''
            
        },
        
        idtype_options: [],
        existingSubmit:false,
        tabIndex:1,
        existinguser:{},
        filteredroles:[],
        last_inserted_id:'',
      
      }
    },
    mounted() {
     
    },
    methods: {
        getRoleChange: function(event){
            console.log(event.target.value);
            if (event.target.value == 2) {
              this.agancy_next = true;
            }else{
              this.agancy_next = false;
            }
        },

        onSubmit(e) {
            console.log(this.form);
            axios.post('/api/item/store-update', this.form).then((response) => {
                  if(response.data.error == 0){
                    this.last_inserted_id = response.data.user_id;
                    
                     $('.modal').modal('hide');
                    for (var key in this.form) {
                      this.form[key] = null;
                    }
                    this.$emit('updatetable')
                  }else{
                    alert(response.data.error);
                  }
                 
                });
        },
        },
    watch: {
      'data'(val) {
            for (var key in this.form) {
              this.form[key] = this.data[key];
            }
        }

    }
  });
   
    //////////////
  new Vue({
    el: '#indexItemController',
  });
}