if (document.querySelector('#indexInvoiceprocessController')) {
  Vue.component('index-invoiceprocess', {
    template: '#index-invoiceprocess-template',
    data() {
      return {
            list:'',
            search: {
            bill_date:'',
            cancelled_at: '',
            cancelled_by: '',
            deleted: '',
            discount_amount: '',
            discount_amount_type: null,
            discount_type: null,
            due_date: '',
            due_reminder_date: null,
            invoice_id: '',
            last_email_sent_date: null,
            next_recurring_date: null,
            no_of_cycles: null,
            no_of_cycles_completed: null,
            note: null,
            property_id: '',
            recurring: null,
            recurring_invoice_id: null,
            recurring_reminder_date: null,
            repeat_every: null,
            repeat_type: null,
            sender_id: '',
            status: '',
            
            taxable_amount: null,
            payable_amount: null,
            unit_id: '',
             
            },
            formdata: {},
            filterchanged: false,
            showdata: {},
            all_active_taxes:[],
            invice_details:{},
            invoice_property:{},
            invoice_unit:{},
            tenancies:{},
            is_have_tenancies:false,
            items:{},
            invoiceitems:[],
            grand_total:0,
            sub_total:0,
            view_id:'',
            rental:0,
            submitted_invoice:true,
            invoice_taxable_amount:0,
            invoice_payable_amount:0,
        }
    },
    mounted() {
        var view_id = window.location.pathname.split('/')[3];
         this.view_id  = view_id;
        this.fetchTable(view_id);

    },
    methods: {
      fetchTable() {
       
        this.searching = true;
      
        let data = {
            
            invoice_id: this.search.invoice_id,
            name: this.search.name,
            bill_date: this.search.bill_date,
            due_date: this.search.due_date,
            status: this.search.status,
            unit_id: this.search.unit_id,

        };
        axios.get(
          // subject to change (search list and pagination)
          '/invoice/getdata/'+ this.view_id 
        ).then((response) => {
            const result            = response.data.invoice;
            const invoice_property  = response.data.invoice_property;
            const invoice_unit      = response.data.invoice_unit;
            const tenancies         = response.data.tenancies;
            const is_have_tenancies = response.data.is_have_tenancies;
            const items             = response.data.items;
            const invoiceitems      = response.data.invoiceitems;
            const grand_total       = response.data.grand_total;
            const sub_total         = response.data.sub_total;
            const invoice_taxable_amount         = response.data.invoice_taxable_amount;
            const invoice_payable_amount         = response.data.invoice_payable_amount;
            
           
          
          if (result) {
            this.invice_details     = result;
            this.invoice_property   = invoice_property;
            this.tenancies          = tenancies;
            this.invoice_unit       = invoice_unit;
            this.is_have_tenancies  = is_have_tenancies;
            this.items              = items;
            this.invoiceitems       = invoiceitems;
            this.sub_total          = sub_total;
            this.grand_total        = grand_total;
             this.invoice_taxable_amount        = invoice_taxable_amount;
              this.invoice_payable_amount        = invoice_payable_amount;
          
          }
        });
        this.searching = false;
       
      },
      searchData() {
        
        this.fetchTable();
        
      },
      sortBy(sortkey) {
       
        this.fetchTable();
      },
      createSingleEntry() {
        this.formdata = '';
      },
      editSingleEntry(data) {
        
        this.formdata = '';
        console.log(data);
        this.rental = data.amount;
        this.formdata = data
      },
      deactivateSingleEntry(data) {
        var approval = confirm('Are you sure to delete ' + data.invoice_item_id + '?')
        if (approval) {
          axios.delete('/api/invoice-item/' + data.invoice_item_id + '/delete').then((response) => {
            this.searchData();
           
          });
        } else {
          return false;
        }
      },
      showUserData(data) {
       
        this.showdata = '';
        this.showdata = data
      },
     
     submitInvoiceItem() {
           axios.get('/api/invoice/' + this.view_id + '/submited').then((response) => {
            
            
            if (response.data.status = 'success') {
                this.searchData();
                alert('invoice Submitted successfully');
                this.submitted_invoice = false;
            } else {
                alert('Unable to submit invoice items');
                this.submitted_invoice = true;
            }
               
          });
       
      },
      
       

     
     

    },
    watch: {
      'selected_page'(val) {
        var view_id = window.location.pathname.split('/')[3];
        this.view_id = view_id;
        this.fetchTable();
      }
    }
  });

  Vue.component('form-invoiceprocess', {
    template: '#form-invoiceprocess-template',
    props: ['data'],
    
    data() {
      
      return {
        form: {
            invoice_item_id     :  '',
            item_id      :  '',
            tax   : '',
            tax_two       : '',
            description:''
           
        },
        tabIndex:1,
        last_inserted_id:'',
        property_options: [],
        unit_options:[],
        all_active_taxes:[],
        items:{},
        tenancies:{},
        rental:0,
        arc_status:0,
        invoice_id:'',
        is_item_rental:false,
      
      }
    },
    mounted() {
        this.fetchPropertySearchOptions();
        this.fetchUnitSearchOptions();
        var view_id = window.location.pathname.split('/')[3];
        this.invoice_id =view_id;
        this.getAllItems();
        this.getTenancies(view_id);
         this.getAllTaxes();
    },
    methods: {
       
        onSubmit(e) {
            console.log(this.form);
            axios.post('/api/invoice-item/store-update', {invoice_id:this.invoice_id, form:this.form}).then((response) => {
                  if(response.data.status == 'success'){
                    this.last_inserted_id = response.data;
                    
                     $('.modal').modal('hide');
                     //window.location.reload()
                    // this.searchData();
                    for (var key in this.form) {
                      this.form[key] = null;
                    }
                    this.$emit('updatetable')
                  }else{
                    alert(response.data.message);
                  }
                 
                });
            },
            fetchPropertySearchOptions() {
                axios.get('/api/properties/all').then((response) => {
                  this.property_options = response.data
                });
              },
              fetchUnitSearchOptions() {
                axios.get('/api/units/all').then((response) => {
                  this.unit_options = response.data
                });
              },
              getAllTaxes(data) {
       
          axios.get('/api/active/taxes').then((response) => {
            this.all_active_taxes = response.data;
            
          });
      
      },
        getAllItems() {
           
              axios.get('/invoice/getitems').then((response) => {
                this.items = response.data;
                
              });
          
          },
          getTenancies(id) {
              axios.get('/invoice/get-invoice-tenancy/'+id).then((response) => {
                this.tenancies = response.data;
                
              });
          
          },

            getTenancyDetails(event){
                var event_data = event.target.value.split("_");
                this.rental     = event_data[0];
                this.arc_status = event_data[1];
            },
             getItemType(event){
                var item_type = event.target.value;
               
                if (item_type == 1) {this.is_item_rental = true;}else{this.is_item_rental = false;}
               
            },
        },
    watch: {
      'data'(val) {
            for (var key in this.form) {
              this.form[key] = this.data[key];
            }
        }

    }
  });
   
    //////////////
  new Vue({
    el: '#indexInvoiceprocessController',
  });
}