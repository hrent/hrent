if (document.querySelector('#indexInvoiceController')) {
  Vue.component('index-invoice', {
    template: '#index-invoice-template',
    data() {
      return {
            list: [],
            search: {
              invoice_id: '',
              unit_id: ''
             
            },
            searching: false,
            sortkey: '',
            reverse: false,
            selected_page: '100',
            pagination: {
              total: 0,
              from: 1,
              per_page: 1,
              current_page: 1,
              last_page: 0,
              to: 5
            },
            formdata: {},
            filterchanged: false,
            showdata: {},
            all_active_taxes:[]

        }
    },
    mounted() {
      this.fetchTable();

    },
    methods: {
      fetchTable() {
       
        this.searching = true;
      
        let data = {
            paginate: this.pagination.per_page,
            page: this.pagination.current_page,
            sortkey: this.sortkey,
            reverse: this.reverse,
            per_page: this.selected_page,
            invoice_id: this.search.invoice_id,
            name: this.search.name,
            bill_date: this.search.bill_date,
            due_date: this.search.due_date,
            status: this.search.status,
           unit_id: this.search.unit_id,

        };
        axios.get(
          // subject to change (search list and pagination)
          '/api/invoices?page=' + data.page +
          '&perpage=' + data.per_page +
          '&sortkey=' + data.sortkey +
          '&reverse=' + data.reverse +
          '&invoice_id=' + data.invoice_id +
           '&name=' + data.name +
           '&bill_date=' + data.bill_date +
            '&name=' + data.name +
            '&due_date=' + data.due_date +
            '&status=' + data.status +
          '&unit_id=' + data.unit_id
        ).then((response) => {
          const result = response.data;
          if (result) {
            this.list = result.data;
            this.pagination = {
              total: result.total,
              from: result.from,
              to: result.to,
              per_page: result.per_page,
              current_page: result.current_page,
              last_page: result.last_page,
            }
          }
        });
        this.searching = false;
       
      },
      searchData() {
        this.pagination.current_page = 1;
        this.fetchTable();
        this.filterchanged = false;
      },
      sortBy(sortkey) {
        this.pagination.current_page = 1;
        this.reverse = (this.sortkey == sortkey) ? !this.reverse : false;
        this.sortkey = sortkey;
        this.fetchTable();
      },
      createSingleEntry() {
        this.formdata = '';
      },
      editSingleEntry(data) {

        this.formdata = '';
        this.formdata = data
      },
      showUserData(data) {
       
        this.showdata = '';
        this.showdata = data
      },
      deactivateSingleEntry(data) {
        var approval = confirm('Are you sure to delete ' + data.name + '?')
        if (approval) {
          axios.delete('/api/invoice/' + data.invoice_id + '/deactivate').then((response) => {
            this.searchData();
             
          });
        } else {
          return false;
        }
      },
      restoreSingleEntry(data) {
        var approval = confirm('Are you sure to restore ' + data.name + '?')
        if (approval) {
          axios.get('/api/invoice/' + data.invoice_id + '/restore').then((response) => {
            this.searchData();
           
          });
        } else {
          return false;
        }
      },
       changeTaxStatus(data) {
   
          axios.post('/api/tax/status',{
            tax_id : data.tax_id,
            status: data.status
          }).then((response) => {
             this.searchData();
           
          });
       
      },

      onFilterChanged() {
        this.filterchanged = true;
      },
     

    },
    watch: {
      'selected_page'(val) {
        this.selected_page = val;
        this.pagination.current_page = 1;
        this.fetchTable();
      }
    }
  });

  Vue.component('form-invoice', {
    template: '#form-invoice-template',
    props: ['data'],
    
    data() {
      
      return {
        form: {
            bill_date     :  '',
            due_date      :  '',
            property_id   : '',
            unit_id       : '',
            recuring      :  '',
            repeat_every  :  '',
            repeat_type   :  '',
            no_of_cycles  : '',
            invoice_id    : '',
            tax:'',
            tax_two:''
        },
        tabIndex:1,
        last_inserted_id:'',
        property_options: [],
        unit_options:[],
        all_active_taxes:[],
      
      }
    },
    mounted() {
     this.fetchPropertySearchOptions();
     this.fetchUnitSearchOptions();
      this.getAllTaxes();
    },
    methods: {
        getRoleChange: function(event){
            console.log(event.target.value);
            if (event.target.value == 2) {
              this.agancy_next = true;
            }else{
              this.agancy_next = false;
            }
        },
        onSubmit(e) {
            console.log(this.form);
            axios.post('/api/invoice/store-update', this.form).then((response) => {
                  if(response.data.status == 'success'){
                    this.last_inserted_id = response.data.tax_id;
                    
                     $('.modal').modal('hide');
                    window.location.href=response.data.redirect_to;
                    for (var key in this.form) {
                      this.form[key] = null;
                    }
                    this.$emit('updatetable')
                  }else{
                    alert(response.data.error);
                  }
                 
                });
            },
            fetchPropertySearchOptions() {
                axios.get('/api/properties/all').then((response) => {
                  this.property_options = response.data
                });
              },
              fetchUnitSearchOptions() {
                axios.get('/api/units/all').then((response) => {
                  this.unit_options = response.data
                });
              },

      getAllTaxes(data) {
       
          axios.get('/api/active/taxes').then((response) => {
            this.all_active_taxes = response.data;
            //window.location.reload()
          });
      
      },
        },
    watch: {
      'data'(val) {
            for (var key in this.form) {
              this.form[key] = this.data[key];
            }
        }

    }
  });
   
    //////////////
  new Vue({
    el: '#indexInvoiceController',
  });
}