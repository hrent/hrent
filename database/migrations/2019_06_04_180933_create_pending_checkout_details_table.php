<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingCheckoutDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_checkout_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('insurance_id');
            $table->string('block_number')->nullable();
            $table->string('unit_number')->nullable();
            $table->string('address')->nullable();
            $table->string('label')->nullable();
            $table->string('value')->nullable();
            $table->string('tax_amout');
            $table->string('sub_total');
            $table->string('total_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_checkout_details');
    }
}
