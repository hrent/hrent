<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('invoice_id');
            $table->integer('property_id'); //from property table
            $table->integer('unit_id'); //from unit table
            $table->integer('tenancy_id')->nullable();//from tenancy table
            $table->enum('is_rental', [0 ,1])->default(1);
            $table->integer('sender_id');
            $table->date('bill_date')->nullable();
            $table->date('due_date')->nullable();
            $table->text('note')->nullable();
            $table->date('last_email_sent_date')->nullable();
            $table->enum('status', ['draft' , 'paid', 'failed', 'cancelled', 'overdue', 'pending'])->default('draft');
            $table->integer('recurring')->nullable();
            $table->integer('recurring_invoice_id')->nullable();
            $table->integer('repeat_every')->nullable();
            $table->enum('repeat_type', ['days' , 'weeks', 'months', 'years'])->nullable();;
            $table->integer('no_of_cycles')->nullable();
            $table->date('next_recurring_date')->nullable();
            $table->integer('no_of_cycles_completed')->nullable();
            $table->date('due_reminder_date')->nullable();
            $table->date('recurring_reminder_date')->nullable();
            $table->double('subtotal_amount', 8, 2)->nullable();
            $table->double('taxable_amount', 8, 2)->nullable();
            $table->double('discount_amount', 8, 2)->nullable();
            $table->double('payable_amount', 8, 2)->nullable();
            $table->enum('is_discount', [0 ,1])->default(0);
            $table->bigInteger('coupons_id')->nullable();
            $table->datetime('cancelled_at')->nullable();
            $table->integer('cancelled_by')->nullable();
            $table->integer('deleted')->default(0);
            $table->integer('invoice_taxes_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
