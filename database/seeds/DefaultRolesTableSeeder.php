<?php
use Illuminate\Database\Seeder;

class DefaultRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Role Types
         *
         */
        $RoleItems = [
             [
                'name'        => 'Admin',
                'slug'        => 'admin',
                'description' => 'Admin Role',
                'level'       => 5,
            ],
            [
                'name'        => 'Agency Admin',
                'slug'        => 'agency',
                'description' => 'Agency Admin Role',
                'level'       => 1,
            ],
            [
                'name'        => 'Agent',
                'slug'        => 'agent',
                'description' => 'Agent Role',
                'level'       => 1,
            ],
             [
                'name'        => 'Tenant',
                'slug'        => 'tenant',
                'description' => 'Tenant Role',
                'level'       => 1,
            ],
             [
                'name'        => 'Beneficiary',
                'slug'        => 'beneficiary',
                'description' => 'Beneficiary Role',
                'level'       => 1,
            ],
        ];

        /*
         * Add Role Items
         *
         */
        echo "\e[32mSeeding:\e[0m DefaultRoleItemsTableSeeder\r\n";
        foreach ($RoleItems as $RoleItem) {
            $newRoleItem = config('roles.models.role')::where('slug', '=', $RoleItem['slug'])->first();
            if ($newRoleItem === null) {
                $newRoleItem = config('roles.models.role')::create([
                    'name'          => $RoleItem['name'],
                    'slug'          => $RoleItem['slug'],
                    'description'   => $RoleItem['description'],
                    'level'         => $RoleItem['level'],
                ]);
                echo "\e[32mSeeding:\e[0m DefaultRoleItemsTableSeeder - Role:".$RoleItem['slug']."\r\n";
            }
        }
    }
}
