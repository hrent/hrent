<?php

use Illuminate\Database\Seeder;
use App\Tax;

class TaxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void  
     */
    public function run()
    {
        Tax::create([
            'name'         => 'Tax One',
            'percentage'   =>  10,
           
        ]);
        Tax::create([
            'name'         => 'Tax Two',
            'percentage'   =>  20,
           
        ]);

        
    }
}