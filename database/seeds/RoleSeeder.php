<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = Role::create([
            'name' => 'superadmin'
        ]);

        $allPermissionsArr = [];

        foreach(Permission::all() as $permission) {
            array_push($allPermissionsArr, $permission->name);
        }

        $superadmin->givePermissionTo($allPermissionsArr);

        Role::create([
            'name' => 'company'
        ]);

        Role::create([
            'name' => 'admin'
        ]);

        Role::create([
            'name' => 'agent'
        ]);

        Role::create([
            'name' => 'tenant'
        ]);

        Role::create([
            'name' => 'beneficiary'
        ]);
    }
}
