<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProfileSeeder::class);
        $this->call(RaceSeeder::class);
        $this->call(GenderSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PropertytypeSeeder::class);
        $this->call(PropertySeeder::class);
        $this->call(UnitSeeder::class);
        $this->call(IdtypeSeeder::class);
        $this->call(TenantSeeder::class);
        $this->call(BeneficiarySeeder::class);
        $this->call(OperatorsTableSeeder::class);
        $this->call(HappyrentInsurancePlansSeeder::class);
        $this->call(HappyrentSiteSettingSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);
        $this->call(ItemsSeeder::class);
        $this->call(TaxesSeeder::class);
        
    }
}
