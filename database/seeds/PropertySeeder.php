<?php

use Illuminate\Database\Seeder;
use App\Property;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Property::create([
            'name' => 'One Amerin Residence',
            'address1' => 'No 35, Persisiran Perling 1,',
            'address2' => 'Taman Perling',
            'postcode' => '81200',
            'city' => 'JB',
            'state' => 'JHR',
            'propertytype_id' => 2
        ]);

        Property::create([
            'name' => 'Aliff Avenue Residence',
            'address1' => 'Jalan Tampoi',
            'address2' => 'Kawasan Perindustrian Tampoi',
            'postcode' => '81200',
            'city' => 'JB',
            'state' => 'JHR',
            'propertytype_id' => 1
        ]);

        Property::create([
            'name' => 'Bora Residence',
            'address1' => '2009, Lebuhraya Sultan Iskandar,',
            'address2' => 'Danga Bay',
            'postcode' => '80200',
            'city' => 'JB',
            'state' => 'JHR',
            'propertytype_id' => 2
        ]);
    }
}
