<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Brian Lee',
            'email' => 'leehongjie91@gmail.com',
            'phone_number' => '+60167350589',
            'password' => 'brian',
            'profile_id' => 1
        ]);
        DB::table('role_user')->insert([
            'role_id'       =>1,
            'user_id'       => $user->id,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s'),
        ]);
        


    }
}
