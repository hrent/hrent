<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void  
     */
    public function run()
    {
        Item::create([
            'title'         => 'Rental',
            'description'   => 'Rental for Unit',
            'unit_type'     => 'Monthly',
            'item_code'     => 'Rental',
            'status'        => 1,
            'deleted'       => 0
        ]);
        Item::create([
            'title'         => 'Water Bill',
            'description'   => 'Pay Water Bill',
            'unit_type'     => 'Monthly',
            'item_code'     => 'Water Bill',
            'status'        => 1,
            'deleted'       => 0
        ]);
        Item::create([
            'title'         => 'Broadband',
            'description'   => 'Pay For Broadband',
            'unit_type'     => 'Monthly',
            'item_code'     => 'Broadband',
            'status'        => 1,
            'deleted'       => 0
        ]);
        Item::create([
            'title'         => 'Electricity',
            'description'   => 'Pay For Electricity',
            'unit_type'     => 'Monthly',
            'item_code'     => 'Electricity',
            'status'        => 1,
            'deleted'       => 0
        ]);

        
    }
}