<?php

use Illuminate\Database\Seeder;
use App\HappyrentInsurancePlans;

class HappyrentInsurancePlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  
    public function run()
    {
        HappyrentInsurancePlans::create([
            'insurance_plan_name'       => 'Plan A',
            'insurance_plan_price'      => '10 RM',
            'insurance_plan_duration'   => 'Monthly'
        ]);

        HappyrentInsurancePlans::create([
            'insurance_plan_name'       => 'Plan B',
            'insurance_plan_price'      => '20 RM',
            'insurance_plan_duration'   => 'Monthly'
        ]);

       
    }
}
